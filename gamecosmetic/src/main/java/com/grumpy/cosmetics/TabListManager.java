package com.grumpy.cosmetics;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import com.grumpy.cosmetics.util.PlayerBuilder;

import net.minecraft.server.v1_12_R1.ChatComponentText;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerListHeaderFooter;

public class TabListManager {

	public static void setRealPlayersFirst() {
		for (Player players : Bukkit.getOnlinePlayers()) {
			Scoreboard scoreboard = players.getScoreboard();
			Team team;

			String teamname = "a";

			if (scoreboard.getTeam(teamname) != null) {
				team = scoreboard.getTeam(teamname);
			} else {
				team = scoreboard.registerNewTeam(teamname);
			}

			team.addEntry(players.getName());
			players.setScoreboard(scoreboard);
		}
	}

	public static void setRealPlayerFirst(Player player) {
		Scoreboard scoreboard = player.getScoreboard();
		Team team;

		String teamname = "a";

		if (scoreboard.getTeam(teamname) != null) {
			team = scoreboard.getTeam(teamname);
		} else {
			team = scoreboard.registerNewTeam(teamname);
		}

		team.addEntry(player.getName());
		player.setScoreboard(scoreboard);
	}

	public static void addPlayer() {
		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.removePacketToAll();
		}

		PlayerBuilder newFakePlayer = new PlayerBuilder("");
		Cosmetic.fakeplayers.add(newFakePlayer);
		// setFakePlayerLast(newFakePlayer.getPlayer().getBukkitEntity().getPlayer());
		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.sendPacketToAll();
		}
	}

	public static void removePlayer() {
		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.removePacketToAll();
		}

		Cosmetic.fakeplayers.remove(0);

		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.sendPacketToAll();
		}
	}

	public static void firstPlayer(Player player, int amount) {
		for (int i = 0; i < amount; i++) {

			PlayerBuilder fakeplayer = new PlayerBuilder("");
			Cosmetic.fakeplayers.add(fakeplayer);

			fakeplayer.sendPacket(player);
		}
	}

	public static void refreshPlayers() {
		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.removePacketToAll();
		}

		for (PlayerBuilder fakeplayer : Cosmetic.fakeplayers) {
			fakeplayer.sendPacketToAll();
		}
	}

	public static void setHeaderForPlayer(Player player) {
		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter(); // tablist header footer
																								// packet
		try {
			Field a = packet.getClass().getDeclaredField("a");
			a.setAccessible(true);

			// Object header = new ChatComponentText("§3Welcome to Grumpy's §2MMORPG");
			Object header = new ChatComponentText("");
			a.set(packet, header);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
