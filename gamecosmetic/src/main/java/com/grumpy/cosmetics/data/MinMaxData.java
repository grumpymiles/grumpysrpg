package com.grumpy.cosmetics.data;

import org.apache.commons.lang3.StringUtils;

public class MinMaxData {

	public int min;
	public int max;
	
	public MinMaxData(int min, int max)
	{
		this.min = min;
		this.max = max;
	}
	
	public MinMaxData(String minmax)
	{
		if(minmax.contains("-"))
		{
			
			String[] temp = minmax.split("-");
			if(StringUtils.isNumeric(temp[1]) && StringUtils.isNumeric(temp[1]))
			{
				this.min = Integer.min(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
				this.max = Integer.max(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]));
				return;
			}
		} else if(StringUtils.isNumeric(minmax))
		{
			this.min = Integer.parseInt(minmax);
			this.max = this.min;
			return;
		}
		this.min = -1;
		this.max = -1; //Failed return -1
	}
	
	public String toString()
	{
		return min + "-" + max;
	}
}

