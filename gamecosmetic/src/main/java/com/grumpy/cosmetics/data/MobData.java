package com.grumpy.cosmetics.data;

import org.bukkit.entity.Horse;

import com.grumpy.cosmetics.customentities.EntityTypes;

public class MobData {

	private EntityTypes type;
	private String name;
	private Horse.Color breed;
	private int profession;
	private int level;
	private int health;
	private int meeledamage;
	private boolean passive;
	private ItemData drop;
	private MobInventoryData inventory;
	

	public MobData(EntityTypes type) {
		this.type = type;
		this.name = "";
		this.level = 0;
		this.inventory = new MobInventoryData();
	}

	public MobData(EntityTypes type, String name, int level, int health, int meeledamage, boolean passive) {
		this.type = type;
		this.name = name;
		this.level = level;
		this.health = health;
		this.meeledamage = meeledamage;
		this.inventory = new MobInventoryData();
	}

	public MobData(EntityTypes type, String name, int profession, int level, int health, int meeledamage, boolean passive) { // villager
		this.type = type;
		this.name = name;
		this.profession = profession;
		this.level = level;
		this.health = health;
		this.meeledamage = meeledamage;
		this.inventory = new MobInventoryData();
	}

	public MobData(EntityTypes type, String name, Horse.Color breed, int level, int health, int meeledamage, boolean passive) { // horse
		this.type = type;
		this.name = name;
		this.breed = breed;
		this.level = level;
		this.health = health;
		this.meeledamage = meeledamage;
		this.inventory = new MobInventoryData();
	}

	public EntityTypes getType() {
		return type;
	}

	public void setType(EntityTypes type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Horse.Color getBreed() {
		return breed;
	}

	public void setBreed(Horse.Color breed) {
		this.breed = breed;
	}

	public int getLevel() {
		return level;
	}

	public int getProfession() {
		return profession;
	}

	public void setProfession(int profession) {
		this.profession = profession;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setPassive(boolean passive) {
		this.passive = passive;
	}

	public boolean isPassive() {
		return passive;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getMeeleDamage() {
		return meeledamage;
	}

	public void setMeeleDamage(int meeledamage) {
		this.meeledamage = meeledamage;
	}

	public ItemData getDrop() {
		return drop;
	}

	public void setDrop(ItemData drop) {
		this.drop = drop;
	}
	
	public MobInventoryData getInventory() {
		return inventory;
	}
	
	public void setInventory(MobInventoryData inventory) {
		this.inventory = inventory;
	}
}
