package com.grumpy.cosmetics.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.DataBaseManager;
import com.grumpy.cosmetics.particles.ParticleEffect;
import com.grumpy.cosmetics.util.Particles;
import com.grumpy.cosmetics.util.UtilParticles;


public class PlayerData {

	public enum Classes {
		EMPTY,
		WARRIOR,
		SORCERER,
		ARCHER,
		SPEARMAN
	}
	
	
	public int currentPlayerSlot;
	
	private UUID uuid;
	public ParticleEffect currentParticleEffect;

	private boolean isInClassSelection;
	private List<Classes> classes  = new ArrayList<>();
	
	int level;
	int currentXp;
	int nextLevelXp;
	
	public PlayerData(UUID uuid) {
		this.currentPlayerSlot = 1;
		this.uuid = uuid;
		this.isInClassSelection = true;
		for(int i = 0; i < 4; i++)
		{
			if(DataBaseManager.playerDataExists(uuid)) {
				if(DataBaseManager.getSlot(uuid, i+1).equals("")) {
						this.classes.add(i, Classes.EMPTY);
				} else {
					classes.add(Classes.valueOf(DataBaseManager.getSlot(uuid, i+1).split(";")[0].toUpperCase()));					
				}
			} else {
				this.classes.add(i, Classes.EMPTY);
			}
		}
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(uuid);
	}

	public UUID getUuid() {
		return uuid;
	}
	
	public boolean isInClassSelection() {
		return isInClassSelection;
	}
	
	public void setClassSelection(boolean isInClassSelection) {
		this.isInClassSelection = isInClassSelection;
	}

	public void removeParticleEffect() {
		if (currentParticleEffect != null) {
			getPlayer().sendMessage(ChatColor.GREEN + currentParticleEffect.getName() + ChatColor.RED + " Is now not active.");
			currentParticleEffect = null;
		}
	}

	public Classes getSlotClass(int slot) {
		return classes.get(slot-1);
	}

	public void setSlotClass(Classes slotClass, int slot) {
		this.classes.set(slot-1, slotClass);
	}
	
	public int getLevel()
	{
		return this.level;
	}
	
	public void setLevel(int level)
	{
		this.level = level;
	}
	
	public int getCurrentXP()
	{
		return this.currentXp;
	}
	
	public void setCurrentXP(int currentXp)
	{
		this.currentXp = currentXp;
	}
	
	public int getNextLevelXP()
	{
		return this.nextLevelXp;
	}
	
	public void setNextLevelXP(int nextLevelXp)
	{
		this.nextLevelXp = nextLevelXp;
	}
	
	 public void addExp(int amount) {
		 Player p = Bukkit.getPlayer(uuid);
		 currentXp += amount;
	     if(currentXp > nextLevelXp) {
	         level++;
	         currentXp = currentXp - nextLevelXp; // get the extra xp
	         nextLevelXp = ((int)(nextLevelXp * 1.35f));
	         UtilParticles.display(Particles.EXPLOSION_HUGE, p.getEyeLocation());
	         p.setHealth(p.getHealthScale());
	         p.setFoodLevel(20);
	         p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_EXPLODE, 10f, 1f);
	         p.sendMessage(ChatColor.BOLD + "" + ChatColor.DARK_GREEN + "Level up!");
	         p.setLevel(level);
	     }
	     p.setExp((float)currentXp/(float)nextLevelXp);
     }

	public void setPlayer(Player player) {
		this.level = player.getLevel();
		this.currentXp = (int) player.getExp();
		this.nextLevelXp = (int) (100 * (player.getLevel() * 1.35));
	}
	
	public Classes getCurrentClass()
	{
		return classes.get(currentPlayerSlot);
	}
}
