package com.grumpy.cosmetics.data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.grumpy.cosmetics.data.PlayerData.Classes;
import com.grumpy.cosmetics.util.ItemUtil;

public class ItemData {
	
	public enum ItemType {
		ARMOR,
		WEAPON,
		MISC
	}
	
	public enum ItemRarity {
		BASIC,
		COMMON,
		RARE,
		LEGENDARY
	}
	
	private String name;
	private ItemType type;
	private ItemRarity rarity;
	private List<String> lore;
	private Material material;
	private int level;
	private Classes weaponClass;
	
	public ItemData(String name)
	{
		this.name = name;
	}
	
	public ItemData(String name, Material material, ItemType type) {
		this.name = name;
		this.material = material;
		this.setType(type);
		this.level = -1;
	}
	
	public ItemData(String name, Material material, ItemType type, List<String> lore) {
		this.name = name;
		this.material = material;
		this.type = type;
		this.lore = lore;
	}
	
	public ItemData(String name, Material material, ItemType type, int level, List<String> lore) {
		this.name = name;
		this.material = material;
		this.type = type;
		this.lore = lore;
		this.level = level;
	}
	
	public ItemData(String name, Material material, ItemType type, int level, List<String> lore, Classes weaponClass) {
		this.name = name;
		this.material = material;
		this.type = type;
		this.lore = lore;
		this.level = level;
		this.weaponClass = weaponClass;
	}
	
	public ItemStack getItemStack()
	{
		ItemStack result = new ItemStack(material, 1);
		ItemMeta meta = result.getItemMeta();
		
		String temp = "";
		
		List<String> templist = new ArrayList<String>();
		
		if(type == ItemType.MISC) {
			temp += ChatColor.WHITE;
			templist.add(ChatColor.GRAY + "Misc Item");
		}
			
		templist.add(ChatColor.GOLD + "Lvl " + level);
		
		templist.add(ChatColor.RED + weaponClass.toString().substring(0, 1) + weaponClass.toString().substring(1).toLowerCase());
		if(rarity != null)
			switch(rarity)
			{
			case BASIC:
				temp += ChatColor.GRAY;
				templist.add(ChatColor.GRAY + "Basic Item");
				break;
			case COMMON:
				temp += ChatColor.YELLOW;
				templist.add(ChatColor.YELLOW + "Common Item");
				break;
			case RARE:
				temp += ChatColor.BLUE;
				templist.add(ChatColor.BLUE + "Rare item");
				break;
			case LEGENDARY:
				temp += ChatColor.DARK_PURPLE;
				templist.add(ChatColor.DARK_PURPLE + "Legendary Item");
				break;
			}
		
		temp += this.name.replaceAll("-", " ");
		for(int i = 0; i < lore.size(); i++)
			templist.add(ChatColor.GRAY + lore.get(i)); //Lore at the end of the item
		
		meta.setLore(templist);
		meta.setDisplayName(temp);
		result.setItemMeta(meta);
		result = ItemUtil.removeAttributes(result);
		return result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ItemType getType() {
		return type;
	}

	public void setType(ItemType type) {
		this.type = type;
	}

	public ItemRarity getRarity() {
		return rarity;
	}

	public void setRarity(ItemRarity rarity) {
		this.rarity = rarity;
	}

	public List<String> getLore() {
		return lore;
	}

	public void setLore(List<String> lore) {
		this.lore = lore;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public void setMaterial(Material material) {
		this.material = material;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Classes getWeaponClass() {
		return weaponClass;
	}

	public void setWeaponClass(Classes weaponClass) {
		this.weaponClass = weaponClass;
	}
	
	
}

