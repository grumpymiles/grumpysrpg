package com.grumpy.cosmetics.data;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

public class MobInventoryData {

	List<Material> inventory = new ArrayList<Material>();
	
	public MobInventoryData()
	{
		for(int i = 0; i < 5; i++)
		{
			inventory.add(Material.AIR);
		}
	}
	
	public MobInventoryData(List<Material> inventory)
	{
		this.inventory = inventory;
	}
	
	public void setItemInHand(Material item) 
	{
		inventory.set(0, item);
	}
	
	public void setBoots(Material item) 
	{
		inventory.set(1, item);
	}
	
	public void setLeggins(Material item) 
	{
		inventory.set(2, item);
	}
	
	public void setChestPlate(Material item) 
	{
		inventory.set(3, item);
	}
	
	public void setHelmet(Material item) 
	{
		inventory.set(4, item);
	}
	
	public Material getItemInHand()
	{
		return inventory.get(0);
	}
	
	public Material getBoots()
	{
		return inventory.get(1);
	}
	
	public Material getLeggins()
	{
		return inventory.get(2);
	}
	
	public Material getChestPlate()
	{
		return inventory.get(3);
	}
	
	public Material getHelmet()
	{
		return inventory.get(4);
	}
	
	public Material[] getContents()
	{
		return inventory.toArray(new Material[inventory.size()]);
	}
	
	public String serialize()
	{
		String result = "";
		for(int i = 0; i < inventory.size(); i++)
			result += inventory.get(i).toString() + ";";
		
		return result;
	}
	
	public static MobInventoryData deSerialize(String string)
	{
		List<Material> result = new ArrayList<Material>();
		String[] tempString = string.split(";");
		
		for(int i = 0; i < tempString.length; i++)
			result.add(Material.valueOf(tempString[i]));
		
		return new MobInventoryData(result);
	}
}
