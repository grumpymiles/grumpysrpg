package com.grumpy.cosmetics.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Location;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.customentities.CustomEntity;

public class SpawnerData {

	String mobname;
	String name;
	int radius;
	String amount;
	Location location;

	public List<UUID> mobs = new ArrayList<UUID>();
	
	public Cosmetic instance;

	public boolean used = false;

	public SpawnerData(String name) {
		this.name = name;
	}

	public SpawnerData(SpawnerData data, Cosmetic instance) {
		this.name = data.name;
		this.mobname = data.mobname;
		this.radius = data.radius;
		this.amount = data.amount;
		this.location = data.location;
		this.instance = instance;
	}

	public SpawnerData(String name, String mobname, int radius, String amount, Location location, Cosmetic instance) {
		this.instance = instance;
		this.name = name;
		this.mobname = mobname;
		this.radius = radius;
		this.amount = amount;
		this.location = location;
	}

	public void update() {
		if (!amount.contains("-")) {
			double aliveamount = mobs.size();
			if(aliveamount == 0 || ((double) aliveamount/((double) Integer.parseInt(amount))) < 0.50) //once 50 procent of the spawner is dead spawn a new batch
			{
				Random r = new Random();
				int randomamount = 1;
				if(Integer.parseInt(amount) != 1)
					 randomamount = r.nextInt(Integer.parseInt(amount)) + (int) Integer.parseInt(amount) / 2;
				
				for (int i = 0; i < randomamount; i++) {
					float x = (float) (Float.min((float) location.getX() - radius, (float) location.getX() + radius)
							+ r.nextFloat() * (Float.max((float) location.getX() - radius, (float) location.getX() + radius)
									- Float.min((float) location.getX() - radius, (float) location.getX() + radius)));
					float z = (float) (Float.min((float) location.getZ() - radius, (float) location.getZ() + radius)
							+ r.nextFloat() * (Float.max((float) location.getZ() - radius, (float) location.getZ() + radius)
									- Float.min((float) location.getZ() - radius, (float) location.getZ() + radius)));
					Location newLoc = new Location(location.getWorld(), x, location.getY(), z);
					int j = 0;
					while (!isSafe(newLoc)) {
						if (j == 4)
							break;
						j++;
						newLoc.add(0, 1, 0);
					}
					if (isSafe(newLoc)) {
						mobs.add(CustomEntity.spawnMob(newLoc, instance.getMobConfig().getMobData("mobs." + mobname)));
					}
				}
			}
		} else {
			Random r = new Random();
			String[] arrayamount = amount.split("-");
			int amount2 = (int) (Integer.parseInt(arrayamount[0])
					+ r.nextFloat() * (Integer.parseInt(arrayamount[1]) - Integer.parseInt(arrayamount[0])));
			for (int i = 0; i < amount2; i++) {
				float x = (float) (Float.min((float) location.getX() - radius, (float) location.getX() + radius)
						+ r.nextFloat() * (Float.max((float) location.getX() - radius, (float) location.getX() + radius)
								- Float.min((float) location.getX() - radius, (float) location.getX() + radius)));
				float z = (float) (Float.min((float) location.getZ() - radius, (float) location.getZ() + radius)
						+ r.nextFloat() * (Float.max((float) location.getZ() - radius, (float) location.getZ() + radius)
								- Float.min((float) location.getZ() - radius, (float) location.getZ() + radius)));
				Location newLoc = new Location(location.getWorld(), x, location.getY(), z);
				int j = 0;
				while (!isSafe(newLoc)) {
					if (j == 4) //if new location is not safe after four times break
						break;
					j++;
					newLoc.add(0, 1, 0);
				}
				if (isSafe(newLoc)) {
					mobs.add(CustomEntity.spawnMob(newLoc, instance.getMobConfig().getMobData("mobs." + mobname)));
				}
			}
		}
	}

	public boolean isSafe(Location loc) {
		if ((loc.getBlock().getType().isTransparent() || loc.getBlock().isLiquid())
				&& (loc.add(0, 1, 0).getBlock().getType().isTransparent() || loc.add(0, 1, 0).getBlock().isLiquid()))
			return true;

		return false;
	}

	public MobData getMobData() {
		return instance.getMobConfig().getMobData("mobs." + mobname);
	}
	
	public String getMobType() {
		return mobname;
	}

	public SpawnerData setMobType(String mobname) {
		this.mobname = mobname;
		return this; //Faster code in command.ModifyMob.java when renaming
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public String getAmount() {
		return amount;
	}

	public int getIntAmount() {
		return Integer.parseInt(amount);
	}
	
	public void setAmount(String amount) {
		this.amount = amount;
	}
}
