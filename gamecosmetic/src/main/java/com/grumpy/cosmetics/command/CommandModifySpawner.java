package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.SpawnerData;

public class CommandModifySpawner implements CommandExecutor, TabCompleter {
	
	Cosmetic instance;

	public CommandModifySpawner(Cosmetic instance) {
		this.instance = instance;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 1) {
				String name = args[0];
				if(instance.getMobConfig().contains("spawners." + name))
				{
					SpawnerData spawner = instance.getMobConfig().getSpawnerData("spawners." + name, instance);
					switch(args[1].toUpperCase()) //toUpperCase to avoid case sensitivity
					{
					case "MOB":
						if(args.length >= 3)
						{
							if(instance.getMobConfig().contains("mobs." + args[2]))
							{
								spawner.setMobType(args[2]);
								instance.getMobConfig().set("spawners." + name, spawner);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the spawner!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Mob name does not exist!");
								return true;
							}
							
						} else {
							player.sendMessage(ChatColor.GREEN + "The mob name is " + ChatColor.YELLOW + spawner.getMobType());
							return true;
						}
					case "RADIUS":
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								spawner.setRadius(Integer.parseInt(args[2]));
								instance.getMobConfig().set("spawners." + name, spawner);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the spawner!");
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Radius is not numeric!");
								return true;
							}							
						} else {
							player.sendMessage(ChatColor.GREEN + "The radius is " + ChatColor.YELLOW + spawner.getRadius());
							return true;
						}
					case "AMOUNT":
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								spawner.setAmount(args[2]);
								instance.getMobConfig().set("spawners." + name, spawner);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the spawner!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Amount is not numeric!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The amount is " + ChatColor.YELLOW + spawner.getAmount());
							return true;
						}
					case "LOCATION":
						if(args.length >= 3)
						{
							if(args[2].equalsIgnoreCase("here"))
							{
								spawner.setLocation(player.getLocation());
								instance.getMobConfig().set("spawners." + name, spawner);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the spawner!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Sorry I cannot process that. Use '/modifyspawner <name> location here' instead!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The location is " + ChatColor.YELLOW + "(" + spawner.getLocation().getBlockX() + ", " + spawner.getLocation().getBlockY() + ", " + spawner.getLocation().getBlockZ() + ")");
							return true;
						}
					case "NAME":
						if(args.length >= 3)
						{
							if(!instance.getMobConfig().contains("spawners." + args[2]))
							{
								spawner.setName(args[2]);
								instance.getMobConfig().remove("spawners." + name);
								instance.getMobConfig().set("spawners." + name, spawner);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the spawners name to " + args[2] + "!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "The spawner is either already called that or there is already a spawner called that!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The name is " + ChatColor.YELLOW + spawner.getName());
							return true;
						}
					default:
						player.sendMessage(ChatColor.DARK_RED + "Invalid arguments!");
						return true;
					}
				} else {
					player.sendMessage(ChatColor.DARK_RED + "Spawner name does not exist!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Too little arguments!");
				return true;
			}
		}
		return false;
	}
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if(sender instanceof Player)
		{
		    if(command.getName().equalsIgnoreCase("modifyspawner") && args.length == 1)
		    {
		    	List<String> result = new ArrayList<>();
				for (String spawnername : instance.getMobConfig().getConfigurationSection("spawners").getKeys(false)) {
					if(spawnername.toUpperCase().startsWith(args[0].toUpperCase())) // non case sensitivity
					{
						result.add(spawnername);
					}
				}
				return result;
		    }
		    else if(command.getName().equalsIgnoreCase("modifyspawner") && args.length == 2)
		    {
		    	List<String> result = new ArrayList<String>();
				for(String argument : Arrays.asList("MOB", "RADIUS", "AMOUNT", "LOCATION", "NAME"))
				{
					if(argument.startsWith(args[1].toUpperCase()))
					{
						result.add(argument);
					}
				}
				return result;
		    }
			else if(command.getName().equalsIgnoreCase("modifyspawner") && args.length == 3 && args[1].equalsIgnoreCase("location"))
				return Arrays.asList("HERE");
			else if(command.getName().equalsIgnoreCase("modifyspawner") && args.length == 3 && args[1].equalsIgnoreCase("mob"))
				return instance.getMobConfig().getStringList("mobs");
		}
		return null;
	}
}
