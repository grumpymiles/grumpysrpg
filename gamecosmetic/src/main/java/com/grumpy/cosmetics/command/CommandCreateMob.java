package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.customentities.EntityTypes;
import com.grumpy.cosmetics.data.MobData;

public class CommandCreateMob implements CommandExecutor, TabCompleter {

	Cosmetic instance;

	public CommandCreateMob(Cosmetic instance) {
		this.instance = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 5) {
				if (!StringUtils.isNumeric(args[2]) || !StringUtils.isNumeric(args[3]) || !StringUtils.isNumeric(args[4])) {
					return false;
				}
				if(args[1].equals("HORSE") && args.length != 7)
				{
					player.sendMessage(ChatColor.DARK_RED + "You need to specify if the mob is passive or not!");
					return false;
				}
				String name = args[0];
				EntityTypes type = EntityTypes.valueOf("CUSTOM_" + args[1].toUpperCase());
				int level = Integer.parseInt(args[2]);
				int health = Integer.parseInt(args[3]);
				int meeledamage = Integer.parseInt(args[4]);
				int passive = Integer.parseInt(args.length > 6 ? args[6] : args[5]);
				if (passive > 1) return false;
				if (args.length > 6) {
					if(!StringUtils.isNumeric(args[5])) {
						Horse.Color color = Horse.Color.valueOf(args[5]);
						instance.getMobConfig().set("mobs." + name, new MobData(type, name, color, level, health, meeledamage, passive < 1 ? false : true));
						instance.getMobConfig().save();
						player.sendMessage(ChatColor.GREEN + "Mob successfully created!");
						return true;
					} else {
						int profession = Integer.parseInt(args[5]);
						instance.getMobConfig().set("mobs." + name, new MobData(type, name, profession, level, health, meeledamage, passive < 1 ? false : true));
						instance.getMobConfig().save();
						player.sendMessage(ChatColor.GREEN + "Mob successfully created!");
						return true;
					}
				} else {
					instance.getMobConfig().set("mobs." + name, new MobData(type, name, level, health, meeledamage, passive < 1 ? false : true));
					instance.getMobConfig().save();
					player.sendMessage(ChatColor.GREEN + "Mob successfully created!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Too little arguments!");
				return false;
			}
		}

		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("createmob") && args.length == 2) {
					List<String> result = new ArrayList<String>();
					for(EntityTypes type : EntityTypes.values())
					{
						if(type.toString().replaceAll("CUSTOM_", "").startsWith(args[1].toUpperCase()))
						{
							result.add(type.toString().replaceAll("CUSTOM_", ""));
						}
					}
					return result;
			} else if (cmd.getName().equalsIgnoreCase("createmob") && args.length == 6) {
				return Stream.of(Horse.Color.values()).map(Horse.Color::name).collect(Collectors.toList());
			}

		}
		return null;
	}
}
