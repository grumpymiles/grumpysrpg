package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.customentities.EntityTypes;
import com.grumpy.cosmetics.data.MobData;

public class CommandModifyMob implements CommandExecutor, TabCompleter {
	
	Cosmetic instance;

	public CommandModifyMob(Cosmetic instance) {
		this.instance = instance;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length >= 2) {
				String name = args[0];
				if(instance.getMobConfig().contains("mobs." + name))
				{
					MobData mob = instance.getMobConfig().getMobData("mobs." + name);
					switch(args[1].toUpperCase()) //toUpperCase to avoid case sensitivity
					{
					case "TYPE":
						if(args.length >= 3)
						{
							if(EntityTypes.valueOf("CUSTOM_" + args[2].toUpperCase()) != null)
							{
								mob.setType(EntityTypes.valueOf("CUSTOM_" + args[2]));
								instance.getMobConfig().set("mobs." + name, mob);
								instance.refreshSpawners();
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mob type!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Mob type is unvalid!");
								return true;								
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The Mob Type is " + ChatColor.YELLOW + mob.getType().name().replace("CUSTOM_", ""));
							return true;
						}
					case "LEVEL":
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								mob.setLevel(Integer.parseInt(args[2]));
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mob level!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Level must be numeric!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The level is " + ChatColor.YELLOW + mob.getLevel());
							return true;
						}
					case "HEALTH":
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								mob.setHealth(Integer.parseInt(args[2]));
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mob health!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Health must be numeric!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The health is " + ChatColor.YELLOW + mob.getHealth());
							return true;
						}
					case "DAMAGE":
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								mob.setMeeleDamage(Integer.parseInt(args[2]));
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mob damage!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Damage must be numeric!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The damage is " + ChatColor.YELLOW + mob.getMeeleDamage());
							return true;
						}
					case "PASSIVE":
						if(args.length >= 3)
						{
							if((StringUtils.isNumeric(args[2]) && args[2].equalsIgnoreCase("0") || args[2].equalsIgnoreCase("0")) || (args[2].equalsIgnoreCase("false") || args[2].equalsIgnoreCase("true")))
							{
								if(StringUtils.isNumeric(args[2]))
									mob.setPassive(Integer.parseInt(args[2]) == 1);
								else
									mob.setPassive(Boolean.parseBoolean(args[2]));
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mob passiveness!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Invalid argument!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The mob is " + ChatColor.YELLOW + (mob.isPassive() ? "" : "not") + "passive");
							return true;
						}
					case "PROFESSION":
						if(args.length >= 3)
						{
							if(mob.getType() == EntityTypes.CUSTOM_VILLAGER)
							{
								if(StringUtils.isNumeric(args[2]) && Integer.parseInt(args[2]) <= 4 && Integer.parseInt(args[2]) >=0)
								{
									mob.setProfession(Integer.parseInt(args[2]));
									instance.getMobConfig().set("mobs." + name, mob);
									player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the mobs profession!");
									return true;
								} else {
									player.sendMessage(ChatColor.DARK_RED + "Profession must be numeric and be between 0 and 4!");
									return true;
								}
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Mob type must be villager to modify the profession!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The damage is " + ChatColor.YELLOW + mob.getProfession());
							return true;
						}
					case "BREED":
						if(args.length >= 3)
						{
							if(mob.getType() == EntityTypes.CUSTOM_HORSE)
							{
								if(Horse.Color.valueOf(args[2]) != null)
								{
									mob.setBreed(Horse.Color.valueOf(args[2]));
									instance.getMobConfig().set("mobs." + name, mob);
									player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the breed type!");
									return true;								
								} else {
									player.sendMessage(ChatColor.DARK_RED + "Breed type is unvalid!");
									return true;								
								}								
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Mob type must be horse to modify the breed!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The breed type is " + ChatColor.YELLOW + mob.getBreed().name());
							return true;
						}
					case "DROP":
						if(args.length >= 3)
						{
							String itemname = args[2];
							if(instance.getMobConfig().contains("item." + itemname))
							{
								mob.setDrop(instance.getMobConfig().getItemData("item." + itemname));
								player.sendMessage("" + mob.getDrop().getName());
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the item drop!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Item does not exist");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".drop") && instance.getMobConfig().contains("item." + instance.getMobConfig().getString("mobs." + name + ".drop")))
							{
								player.sendMessage(ChatColor.GREEN + "The drop is " + ChatColor.YELLOW + instance.getMobConfig().getString("mobs." + name + ".drop"));
								return true;
							}
						}
					case "ITEMINHAND":
						if(args.length >= 3)
						{
							if(EnumUtils.isValidEnum(Material.class, args[2].toUpperCase()))
							{
								mob.getInventory().setItemInHand(Material.getMaterial(args[2].toUpperCase()));
								
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the item in hand!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Material " + args[2] + " does not exist!");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".inventory") && instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getItemInHand() != Material.AIR)
							{
								player.sendMessage(ChatColor.GREEN + "The helmey is " + ChatColor.YELLOW + instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getHelmet());
								return true;
							} else {
								player.sendMessage(ChatColor.GREEN + "The item in hand is AIR");
								return true;
							}
						}
					case "HELMET":
						if(args.length >= 3)
						{
							if(EnumUtils.isValidEnum(Material.class, args[2].toUpperCase()))
							{
								mob.getInventory().setHelmet(Material.getMaterial(args[2].toUpperCase()));
								
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the helmet!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Material " + args[2] + " does not exist!");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".inventory") && instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getHelmet() != Material.AIR)
							{
								player.sendMessage(ChatColor.GREEN + "The helmet is " + ChatColor.YELLOW + instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getHelmet());
								return true;
							} else {
								player.sendMessage(ChatColor.GREEN + "The Helmet is" + ChatColor.YELLOW + " AIR");
								return true;
							}
						}
					case "CHESTPLATE":
						if(args.length >= 3)
						{
							if(EnumUtils.isValidEnum(Material.class, args[2].toUpperCase()))
							{
								mob.getInventory().setChestPlate(Material.getMaterial(args[2].toUpperCase()));
								
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the chestplate!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Material " + args[2] + " does not exist!");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".inventory") && instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getChestPlate() != Material.AIR)
							{
								player.sendMessage(ChatColor.GREEN + "The chestplate is " + ChatColor.YELLOW + instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getChestPlate());
								return true;
							} else {
								player.sendMessage(ChatColor.GREEN + "The chestplate is" + ChatColor.YELLOW + " AIR");
								return true;
							}
						}
					case "PANTS":
						if(args.length >= 3)
						{
							if(EnumUtils.isValidEnum(Material.class, args[2].toUpperCase()))
							{
								mob.getInventory().setLeggins(Material.getMaterial(args[2].toUpperCase()));
								
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the pants!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Material " + args[2] + " does not exist!");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".inventory") && instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getLeggins() != Material.AIR)
							{
								player.sendMessage(ChatColor.GREEN + "The item in hand is " + ChatColor.YELLOW + instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getLeggins());
								return true;
							} else {
								player.sendMessage(ChatColor.GREEN + "The Helmet is" + ChatColor.YELLOW + " AIR");
								return true;
							}
						}
					case "BOOTS":
						if(args.length >= 3)
						{
							if(EnumUtils.isValidEnum(Material.class, args[2].toUpperCase()))
							{
								mob.getInventory().setBoots(Material.getMaterial(args[2].toUpperCase()));
								
								instance.getMobConfig().set("mobs." + name, mob);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the boots!");
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Material " + args[2] + " does not exist!");
								return true;
							}
						} else {
							if(instance.getMobConfig().contains("mobs." + name + ".inventory") && instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getBoots() != Material.AIR)
							{
								player.sendMessage(ChatColor.GREEN + "The item in hand is " + ChatColor.YELLOW + instance.getMobConfig().getMobData("mobs." + name + ".inventory").getInventory().getBoots());
								return true;
							} else {
								player.sendMessage(ChatColor.GREEN + "The boots are" + ChatColor.YELLOW + " AIR");
								return true;
							}
						}
					case "RENAME":
						if(args.length >= 3)
						{
							String newname = args[2];
							if(newname.equals(name))
							{
								player.sendMessage(ChatColor.DARK_RED + "Cannot have the same name!");
								return true;
							} else {
								mob.setName(newname);
								instance.getMobConfig().set("mobs." + newname, mob);
								instance.getMobConfig().remove("mobs." + name);
								for(String spawner : instance.getMobConfig().getConfigurationSection("spawners").getKeys(false))
								{
									if(instance.getMobConfig().getSpawnerData("spawners." + spawner, instance).getMobType().equals(name))
									{
										instance.getMobConfig().set("spawners." + spawner, (instance.getMobConfig().getSpawnerData("spawners." + spawner, instance).setMobType(newname)));
									}
								}
								player.sendMessage(ChatColor.YELLOW + "Sucessfully renamed the mob!");
								return true;
							}
							
						} else {							
							player.sendMessage(ChatColor.GREEN + "The name is " + ChatColor.YELLOW + mob.getName());
							return true;
						}
					}
				} else {
					player.sendMessage(ChatColor.DARK_RED + "Mob name doesn't exist");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Too little arguments");
				return true;
			}
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if(command.getName().equalsIgnoreCase("modifymob") && args.length == 1)
		{
			List<String> result = new ArrayList<>();
			for (String mobname : instance.getMobConfig().getConfigurationSection("mobs").getKeys(false)) {
				if(mobname.toString().toUpperCase().startsWith(args[0].toUpperCase())) // non case sensitivity
				{
					result.add(mobname);
				}
			}
			return result;
		}
		else if(command.getName().equalsIgnoreCase("modifymob") && args.length == 2)
		{
			List<String> result = new ArrayList<String>();
			for(String argument : Arrays.asList("TYPE", "LEVEL", "HEALTH", "DAMAGE", "PASSIVE", "PROFESSION", "BREED", "DROP", "ITEMINHAND", "HELMET", "CHESTPLATE", "PANTS", "BOOTS", "RENAME"))
			{
				if(argument.startsWith(args[1].toUpperCase()))
				{
					result.add(argument);
				}
			}
			return result;
		}
		else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("type"))
		{
			List<String> result = new ArrayList<String>();
			for(EntityTypes type : EntityTypes.values())
			{
				if(type.toString().replaceAll("CUSTOM_", "").startsWith(args[2].toUpperCase()))
				{
					result.add(type.toString().replaceAll("CUSTOM_", ""));
				}
			}
			return result;
		} else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("passive"))
			return Arrays.asList("True", "False");
		else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("breed"))
			return Stream.of(Horse.Color.values()).map(Horse.Color::name).collect(Collectors.toList());
		else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("profession"))
			return Arrays.asList("0", "1", "2", "3", "4");
		else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("drop"))
		{
			List<String> result = new ArrayList<>();
			for (String itemname : instance.getMobConfig().getConfigurationSection("item").getKeys(false)) {
				if(itemname.toUpperCase().startsWith(args[2].toUpperCase())) // non case sensitivity
				{
					result.add(itemname);
				}
			}
			return result;
			
		} else if (command.getName().equalsIgnoreCase("modifymob") && args.length == 3 && args[1].equalsIgnoreCase("iteminhand") || args[1].equalsIgnoreCase("helmet") || args[1].equalsIgnoreCase("chestplate") || args[1].equalsIgnoreCase("pants") || args[1].equalsIgnoreCase("boots"))
		{
			List<String> result = new ArrayList<String>();
			for(Material material : Material.values())
			{
				if(material.toString().startsWith(args[2].toUpperCase()))
				{
					result.add(material.toString());
				}
			}
			return result;
			
		} 
			
		return null;
	}
}
