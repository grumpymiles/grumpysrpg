package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.SpawnerData;

public class CommandSpawnerNear implements CommandExecutor {
	
	Cosmetic instance;
	
	public CommandSpawnerNear(Cosmetic instance)
	{
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 1) {
				if(StringUtils.isNumeric(args[0]))
				{
					int radius = Integer.parseInt(args[0]);
					Location loc = player.getLocation();
					List<SpawnerData> spawnersnear = new ArrayList<SpawnerData>();
					for(SpawnerData spawners : instance.spawnerList)
					{
						if(spawners.getLocation().distance(loc) < radius)
							spawnersnear.add(spawners);
					}
					
					for(int i=0;i<20;i++) //clear chat
						player.sendMessage("");
					
					player.sendMessage(ChatColor.YELLOW + "" + spawnersnear.size() + " Spawners near!");
					for(int i = 0; i < spawnersnear.size(); i++)
					{
						player.sendMessage(ChatColor.LIGHT_PURPLE + "Spawner  " + ChatColor.RED + spawnersnear.get(i).getName() + ChatColor.LIGHT_PURPLE + " Mobtype: " + ChatColor.RED + spawnersnear.get(i).getMobType());
					}
					return true;
				} else {
					player.sendMessage(ChatColor.DARK_RED + "Radius must be an integer!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Invalid arguments!");
				return false;
			}
		}
		return false;
	}
}
