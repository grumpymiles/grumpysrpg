package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;

public class CommandRemoveMob implements CommandExecutor, TabCompleter {

	Cosmetic instance;

	public CommandRemoveMob(Cosmetic instance) {
		this.instance = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length == 1) {
				String name = args[0];
				if (instance.getMobConfig().contains("mobs." + name)) {
					instance.getMobConfig().remove("mobs." + name);
					player.sendMessage(ChatColor.GREEN + "Sucessfully removed the following mob: " + name);
					return true;
				} else {
					player.sendMessage(ChatColor.RED + name + " Does not exist in the config.");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Only one argument needed!");
				return true;
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("removemob") && args.length == 1) {
				List<String> result = new ArrayList<>();
				for (String mobname : instance.getMobConfig().getConfigurationSection("mobs").getKeys(false)) {
					if(mobname.toString().toUpperCase().startsWith(args[0].toUpperCase())) // non case sensitivity
					{
						result.add(mobname);
					}
				}
				return result;
			}

		}
		return null;
	}
}
