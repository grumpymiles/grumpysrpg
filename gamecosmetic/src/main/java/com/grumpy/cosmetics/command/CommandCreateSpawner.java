package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.SpawnerData;

public class CommandCreateSpawner implements CommandExecutor, TabCompleter {

	Cosmetic instance;

	public CommandCreateSpawner(Cosmetic instance) {
		this.instance = instance;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 3) {
				if (StringUtils.isNumeric(args[2]) && (StringUtils.isNumeric(args[3]) || args[3].contains("-"))) {
					String name = args[0];
					if (instance.getMobConfig().contains("spawners") && instance.getMobConfig().getConfigurationSection("spawners").getKeys(false).contains(name)) {
						player.sendMessage(ChatColor.RED + "Spawner Already exists!");
						return true;
					}
					String mob = args[1];
					if (instance.getMobConfig().contains("mobs." + mob)) {
						int radius = Integer.parseInt(args[2]);
						String amount = args[3];
						Location loc = player.getLocation();
						instance.getMobConfig().set("spawners." + name,
								new SpawnerData(name, mob, radius, amount, loc, instance));

						instance.refreshSpawners();
						player.sendMessage(ChatColor.GREEN + "Successfully created a spawner!");
						return true;
					} else {
						player.sendMessage(ChatColor.RED + "You have to choose an existing mob name!");
						return true;
					}
				} else {
					player.sendMessage(ChatColor.RED + "Argument 2 and 3 needs to be numeric!");
					return false;
				}
			} else {
				player.sendMessage(ChatColor.RED + "Too Little Arguments!");
				return false;
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			if (cmd.getName().equalsIgnoreCase("createspawner") && args.length == 2)
			{
				List<String> result = new ArrayList<>();
				for (String mobname : instance.getMobConfig().getConfigurationSection("mobs").getKeys(false)) {
					if(mobname.toString().toUpperCase().startsWith(args[1].toUpperCase()))
					{
						result.add(mobname);
					}
				}
				return result;
			}
		}
		return null;
	}

}
