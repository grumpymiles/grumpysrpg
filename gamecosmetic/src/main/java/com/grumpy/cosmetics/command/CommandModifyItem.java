package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.ItemData;
import com.grumpy.cosmetics.data.ItemData.ItemRarity;
import com.grumpy.cosmetics.data.ItemData.ItemType;
import com.grumpy.cosmetics.data.PlayerData.Classes;

public class CommandModifyItem implements CommandExecutor, TabCompleter {

	Cosmetic instance;
	
	public CommandModifyItem(Cosmetic instance)
	{
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length > 2)
			{
				String name = args[0];
				if(instance.getMobConfig().contains("item." + name))
				{
					ItemData item = instance.getMobConfig().getItemData("item." + name);
					switch(args[1].toUpperCase()) //toUpperCase to avoid case sensitivity
					{
					case "MATERIAL":
						if(args.length >= 3)
						{
							if(Material.valueOf(args[2].toUpperCase()) != null)
							{
								item.setMaterial(Material.valueOf(args[2]));
								instance.getMobConfig().set("item." + name, item);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified item material!");
								player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "The material is invalid!");
								return true;								
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The material is " + item.getMaterial().name());
							return true;
						}
				case "TYPE":
					if(args.length >= 3)
					{
						if(ItemType.valueOf(args[2].toUpperCase()) != null)
						{
							item.setType(ItemType.valueOf(args[2]));
							instance.getMobConfig().set("item." + name, item);
							player.sendMessage(ChatColor.YELLOW + "Sucessfully modified item type!");
							player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
							return true;
						} else {
							player.sendMessage(ChatColor.DARK_RED + "The type is invalid!");
							return true;								
						}
					} else {
						player.sendMessage(ChatColor.GREEN + "The type is " + item.getType().name());
						return true;
					}
				case "LORE":
					if(args.length < 3)
					{
						if(args[2].equalsIgnoreCase("add"))
						{
							List<String> list = item.getLore();
							for(int i = 4; i < args.length; i++)
								list.add(args[args.length - 1]);
							instance.getMobConfig().set("item." + name, item);
							player.sendMessage(ChatColor.YELLOW + "Sucessfully added item lore!");
							player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
						} else if(args.length >= 3){
							if(!args[2].equals("-")) // no need for ignorecase
								item.setLore(Arrays.asList(args[2]));
							else
								item.setLore(Arrays.asList(""));
							instance.getMobConfig().set("item." + name, item);
							player.sendMessage(ChatColor.YELLOW + "Sucessfully modified item lore!");
							player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
							return true;
						}
						
					} else {
						player.sendMessage(ChatColor.GREEN + "The lore is " + item.getLore());
						return true;
					}
				case "RARITY":
					if(args.length >= 3)
					{
						if(ItemRarity.valueOf(args[2].toUpperCase()) != null)
						{
							item.setRarity(ItemRarity.valueOf(args[2]));
							instance.getMobConfig().set("item." + name, item);
							player.sendMessage(ChatColor.YELLOW + "Sucessfully modified item rarity!");
							player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
							return true;
						} else {
							player.sendMessage(ChatColor.DARK_RED + "The item rarity is invalid!");
							return true;								
						}
					} else {
						player.sendMessage(ChatColor.GREEN + "The item rarity is " + item.getRarity().name());
						return true;
					}
				case "LEVEL":
					if(item.getType() != ItemType.MISC)
					{
						if(args.length >= 3)
						{
							if(StringUtils.isNumeric(args[2]))
							{
								item.setLevel(Integer.parseInt(args[2]));
								instance.getMobConfig().set("item." + name, item);
								player.sendMessage(ChatColor.YELLOW + "Sucessfully modified the item level!");
								player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
								return true;
							} else {
								player.sendMessage(ChatColor.DARK_RED + "Level must be numeric!");
								return true;
							}
						} else {
							player.sendMessage(ChatColor.GREEN + "The level is " + item.getLevel());
							return true;
						}						
					} else {
						player.sendMessage(ChatColor.DARK_RED + "Item type must be Armor or Weapon");
					}
				case "CLASS":
					if(args.length >= 3)
					{
						if(Classes.valueOf(args[2].toUpperCase()) != null && item.getType() == ItemType.WEAPON)
						{
							item.setWeaponClass(Classes.valueOf(args[2]));
							instance.getMobConfig().set("item." + name, item);
							player.sendMessage(ChatColor.YELLOW + "Sucessfully modified item class!");
							player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
							return true;
						} else {
							player.sendMessage(ChatColor.DARK_RED + "The item class is invalid!");
							return true;								
						}
					} else {
						player.sendMessage(ChatColor.GREEN + "The item class is " + item.getWeaponClass().name());
						return true;
					}
				}
				} else {
					player.sendMessage(ChatColor.DARK_RED + "Item name doesn't exist");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Not enough arguments!");
			}
		}
		return false;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if(command.getName().equalsIgnoreCase("modifyitem") && args.length == 1) {
			List<String> result = new ArrayList<>();
			for (String item : instance.getMobConfig().getConfigurationSection("item").getKeys(false)) {
				if(item.toString().toUpperCase().startsWith(args[0].toUpperCase())) // non case sensitivity
				{
					result.add(item);
				}
			}
			return result;
		}
		else if(command.getName().equalsIgnoreCase("modifyitem") && args.length == 2)
		{
			List<String> list = Arrays.asList("MATERIAL", "TYPE", "LORE", "RARITY", "LEVEL", "CLASS");
			List<String> result = new ArrayList<String>();
			for(String argument : list)
			{
				if(argument.startsWith(args[1].toUpperCase()))
				{
					result.add(argument);
				}
			}
			return result;
		}
		else if (command.getName().equalsIgnoreCase("modifyitem") && args.length >= 2 && args[1].equalsIgnoreCase("LORE"))
			return Arrays.asList("ADD", "-");
		else if (command.getName().equalsIgnoreCase("modifyitem") && args.length >= 2 && args[1].equalsIgnoreCase("MATERIAL"))
		{
			List<String> result = new ArrayList<String>();
			for(Material material : Material.values())
			{
				if(material.toString().startsWith(args[2].toUpperCase()))
				{
					result.add(material.toString());
				}
			}
			return result;
		} else if(command.getName().equalsIgnoreCase("modifyitem") && args.length >= 2 && args[1].equalsIgnoreCase("CLASS")) {
				List<String> result = new ArrayList<String>();
				for(Classes classes : Classes.values())
				{
					if(classes.toString().startsWith(args[2].toUpperCase()))
					{
						result.add(classes.toString());
					}
				}
				return result;
			} else if(command.getName().equalsIgnoreCase("modifyitem") && args.length >= 2 && args[1].equalsIgnoreCase("RARITY"))
			return Stream.of(ItemRarity.values()).map(ItemRarity::name).collect(Collectors.toList());
		else if(command.getName().equalsIgnoreCase("modifyitem") && args.length >= 2 && args[1].equalsIgnoreCase("TYPE"))
			return Stream.of(ItemType.values()).map(ItemType::name).collect(Collectors.toList());
		
		return null;
	}
}
