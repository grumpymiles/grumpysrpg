package com.grumpy.cosmetics.command;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.ItemData;
import com.grumpy.cosmetics.data.ItemData.ItemRarity;
import com.grumpy.cosmetics.data.ItemData.ItemType;
import com.grumpy.cosmetics.data.PlayerData.Classes;

public class CommandCreateItem implements CommandExecutor, TabCompleter {

	Cosmetic instance;
	
	public CommandCreateItem(Cosmetic instance)
	{
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player) {
			Player player = (Player)sender;
			if(args.length > 2)
			{
				String name = args[0];
				if(Material.getMaterial(args[1].toUpperCase()) != null)
				{
					Material material = Material.getMaterial(args[1].toUpperCase());
					if(ItemType.valueOf(args[2]) != null)
					{
						ItemData item = new ItemData(name, material, ItemType.valueOf(args[2]));
						
						if(item.getType() != ItemType.MISC)
						{
							if(args.length >= 4 && ItemRarity.valueOf(args[3]) != null)
							{
								item.setRarity(ItemRarity.valueOf(args[3]));
								if(args.length >= 5 && StringUtils.isNumeric(args[4]))
									item.setLevel(Integer.parseInt(args[4]));
								
								if(item.getType() == ItemType.WEAPON && args.length == 6 && Classes.valueOf(args[5]) != null)
								{
									item.setWeaponClass(Classes.valueOf(args[5]));
								} else {
									player.sendMessage(ChatColor.DARK_RED + "You need to specify the class");
									return false;
								}
							}							
						} else {
							player.sendMessage(ChatColor.DARK_RED + "You can't create Weapons or Armor without specifying level, rarity, etc!");
							return false;
						}
						instance.getMobConfig().set("item." + name, item);
						player.sendMessage(ChatColor.GREEN + "Sucessfully created item");
						player.getInventory().addItem(instance.getMobConfig().getItemData("item." + name).getItemStack());
						return true;
					} else {
						player.sendMessage(ChatColor.DARK_RED + "Item type unvalid");
					}
				} else {
					player.sendMessage(ChatColor.DARK_RED + "Unvalid Material try using TAB!");
					return true;
				}
			} else {
				player.sendMessage(ChatColor.DARK_RED + "Too little arguments!");
				return false;
			}
		}
		return false;
		
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		if (command.getName().equalsIgnoreCase("createitem") && args.length == 2) {
			List<String> result = new ArrayList<String>();
			for(Material material : Material.values())
			{
				if(material.toString().startsWith(args[1].toUpperCase()))
				{
					result.add(material.toString());
				}
			}
			return result;
			
		} else if(command.getName().equalsIgnoreCase("createitem") && args.length == 3) {
			List<String> result = new ArrayList<String>();
			for(ItemType itemType : ItemType.values())
			{
				if(itemType.toString().startsWith(args[2].toUpperCase()))
				{
					result.add(itemType.toString());
				}
			}
			return result;
		} else if(command.getName().equalsIgnoreCase("createitem") && args.length == 4) {
			List<String> result = new ArrayList<String>();
			for(ItemRarity itemrarity : ItemRarity.values())
			{
				if(itemrarity.toString().startsWith(args[3].toUpperCase()))
				{
					result.add(itemrarity.toString());
				}
			}
			return result;
		} else if(command.getName().equalsIgnoreCase("createitem") && args[2].toUpperCase().equals("WEAPON") && args.length == 6) {
			List<String> result = new ArrayList<String>();
			for(Classes classes : Classes.values())
			{
				if(classes.toString().startsWith(args[5].toUpperCase()))
				{
					result.add(classes.toString());
				}
			}
			return result;
		}
		return null;
	}
}
