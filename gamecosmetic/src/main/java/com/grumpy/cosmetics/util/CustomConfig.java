package com.grumpy.cosmetics.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Horse;
import org.bukkit.plugin.java.JavaPlugin;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.customentities.EntityTypes;
import com.grumpy.cosmetics.data.ItemData;
import com.grumpy.cosmetics.data.ItemData.ItemRarity;
import com.grumpy.cosmetics.data.ItemData.ItemType;
import com.grumpy.cosmetics.data.MobData;
import com.grumpy.cosmetics.data.MobInventoryData;
import com.grumpy.cosmetics.data.PlayerData.Classes;
import com.grumpy.cosmetics.data.SpawnerData;

public class CustomConfig extends YamlConfiguration {   
   
    private File file;
    private String defaults;
    private JavaPlugin plugin;
   
    Cosmetic instance;
    
    /**
     * Creates new PluginFile, without defaults
     * @param plugin - Your plugin
     * @param fileName - Name of the file
     */
    public CustomConfig(JavaPlugin plugin, String fileName, Cosmetic instance) {
        this(plugin, fileName, null, instance);
    }
   
    /**
     * Creates new PluginFile, with defaults
     * @param plugin - Your plugin
     * @param fileName - Name of the file
     * @param defaultsName - Name of the defaults
     */
    public CustomConfig(JavaPlugin plugin, String fileName, String defaultsName, Cosmetic instance) {
    	this.instance = instance;
        this.plugin = plugin;
        this.defaults = defaultsName;
        this.file = new File(plugin.getDataFolder(), fileName);
        reload();
    }
   
    /**
     * Reload configuration
     */
    public void reload() {
       
        if (!file.exists()) {
           
            try {
                file.getParentFile().mkdirs();
                file.createNewFile();
               
            } catch (IOException exception) {
                exception.printStackTrace();
                plugin.getLogger().severe("Error while creating file " + file.getName());
            }
           
        }
       
        try {
            load(file);
           
            if (defaults != null) {
                InputStreamReader reader = new InputStreamReader(plugin.getResource(defaults));
                FileConfiguration defaultsConfig = YamlConfiguration.loadConfiguration(reader);       
               
                setDefaults(defaultsConfig);
                options().copyDefaults(true);
               
                reader.close();
                save();
            }
       
        } catch (IOException exception) {
            exception.printStackTrace();
            plugin.getLogger().severe("Error while loading file " + file.getName());
           
        } catch (InvalidConfigurationException exception) {
            exception.printStackTrace();
            plugin.getLogger().severe("Error while loading file " + file.getName());
           
        }
       
    }
    
    /**
     * Set Mobdata
     */
    
    public void set(String path, MobData mob) {
    	if(mob.getBreed() != null)
    		set(path + ".breed", mob.getBreed().toString());
    	if(mob.getDrop() != null)
    		set(path + ".drop", mob.getDrop().getName());
    	if(mob.getInventory() != null)
    		set(path + ".inventory", mob.getInventory().serialize());
    	set(path + ".name", mob.getName());
    	set(path + ".level", mob.getLevel());
    	set(path + ".health", mob.getHealth());
    	set(path + ".meeledamage", mob.getMeeleDamage());
    	set(path + ".type", mob.getType().toString());
    	set(path + ".profession", mob.getProfession()); //never null even if you dont set it somehow
    	set(path + ".passive", mob.isPassive());
    	save();
    }
    
    /**
     * Get Mobdata
     */
   
    public MobData getMobData(String path) {
    	MobData result = new MobData(EntityTypes.valueOf(getString(path + ".type")));
    	if(contains(path + ".breed"))
    		result.setBreed(Horse.Color.valueOf(getString(path + ".breed")));
    	if(contains(path + ".drop") && contains("item." + getString(path + ".drop")))
    		result.setDrop(getItemData("item." + getString(path + ".drop")));
    	if(contains(path + ".inventory"))
    		result.setInventory(MobInventoryData.deSerialize(getString(path + ".inventory")));
    	result.setName(getString(path + ".name"));
    	result.setLevel(getInt(path + ".level"));
    	result.setHealth(getInt(path + ".health"));
    	result.setMeeleDamage(getInt(path + ".meeledamage"));
    	result.setProfession(getInt(path + ".profession"));
    	result.setPassive(getBoolean(path + ".passive"));
    	return result;
    }
    
    
    /**
     * Set SpawnerData
     */
    
    public void set(String path, SpawnerData spawner) {
    	path = path.substring(0,9); //removes the spawner name
    	set(path + spawner.getName() + ".name", spawner.getName());
		set(path + spawner.getName() + ".mobname", spawner.getMobType());
		set(path + spawner.getName() + ".radius", spawner.getRadius());
		set(path + spawner.getName() + ".amount", spawner.getAmount());
		set(path + spawner.getName() + ".location", spawner.getLocation());
		save();
    }
    
    
    
    /**
     * Get SpawnerData
     */
   
    public SpawnerData getSpawnerData(String path, Cosmetic instance) {
    	SpawnerData result = new SpawnerData(path + ".name");
    	result.instance = instance;										//TODO Try to get rid of this, SpawnerData uses it
    	result.setName(getString(path + ".name"));
    	result.setLocation((Location) get(path + ".location"));
    	result.setMobType(getString(path + ".mobname"));
    	result.setAmount(getString(path + ".amount"));
    	result.setRadius(getInt(path + ".radius"));
    	return result;
    }
    
    /**
     * Set ItemData
     */
    
    public void set(String path, ItemData item) {
    	set(path + ".name", item.getName());
    	set(path + ".material", item.getMaterial().toString());
    	set(path + ".type", item.getType().toString());
    	
    	if(item.getLevel() != -1)
    		set(path + ".level", item.getLevel());
    	
    	if(item.getLore() != null)
    		set(path + ".lore", item.getLore());
    	else
    		set(path + ".lore", "");
    	if(item.getRarity() != null)
    		set(path + ".rarity", item.getRarity().toString());	
    	if(item.getWeaponClass() != null)
    		set(path + ".class", item.getWeaponClass().toString());
    	save();
    }
    
    /**
     * Get ItemData
     */
    
    public ItemData getItemData(String path) {
    	ItemData result = new ItemData(getString(path + ".name"));
    	result.setName(getString(path + ".name"));
    	result.setMaterial(Material.valueOf(getString(path + ".material")));
    	result.setType(ItemType.valueOf(getString(path + ".type")));
    	result.setLore(getStringList(path + ".lore"));
    	if(contains(path + ".level"))
    		result.setLevel(getInt(path + ".level"));
    	if(contains(path + ".rarity"))
    		result.setRarity(ItemRarity.valueOf(getString(path + ".rarity")));
    	if(contains(path + ".class"))
    		result.setWeaponClass(Classes.valueOf(getString(path + ".class")));
    	return result;
    }
    
    public void remove(String path) {
    	set(path, (Object) null);
    	save();
    }
    
    /**
     * Save configuration
     */
    public void save() {
       
        try {
            options().indent(2);
            save(file);
           
        } catch (IOException exception) {
            exception.printStackTrace();
            plugin.getLogger().severe("Error while saving file " + file.getName());
        }
       
    }
    
	public static String getMobConfigName(String str)
	{
		String tempname = str;
		String result = "";
		for(int i = 0; i < tempname.length(); i++)
		{
			char namechar = tempname.charAt(i);
			if(namechar == ' ')
			{
				if(tempname.charAt(i+1) == '[')
					break;
				else
					result += '-';
			} else {
				result += namechar;
			}
		}
		result = ChatColor.stripColor(result); //remove colors from name
		return result;
	}
	
}