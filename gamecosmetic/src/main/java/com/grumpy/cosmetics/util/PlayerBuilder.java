package com.grumpy.cosmetics.util;

import java.lang.reflect.Method;
import java.net.URL;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;

import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.MinecraftServer;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;
import net.minecraft.server.v1_12_R1.PlayerConnection;
import net.minecraft.server.v1_12_R1.PlayerInteractManager;
import net.minecraft.server.v1_12_R1.WorldServer;

public class PlayerBuilder {
	private MinecraftServer server;
	private WorldServer world;
	private UUID uuid;
	private GameProfile profile;
	private PlayerInteractManager manager;
	private int ping;
	private EntityPlayer player;

	private static Method MAKE_REQUEST;

	static {
		try {
			MAKE_REQUEST = YggdrasilAuthenticationService.class.getDeclaredMethod("makeRequest", URL.class,
					Object.class, Class.class);
			MAKE_REQUEST.setAccessible(true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public PlayerBuilder(UUID uuid) {
		this.server = ((CraftServer) Bukkit.getServer()).getServer();
		this.world = server.getWorldServer(0);

		this.uuid = uuid;
		this.profile = new GameProfile(this.uuid, "zzzzzzzz"); // Set the fake player last in the tablist

		this.manager = new PlayerInteractManager(world);
		this.player = new EntityPlayer(this.server, this.world, this.profile, this.manager);
		// this.player.a(this.mode);
		this.player.ping = this.ping;
	}

	public PlayerBuilder(String name) {
		this.server = ((CraftServer) Bukkit.getServer()).getServer();
		this.world = server.getWorldServer(0);

		this.uuid = UUID.randomUUID();
		this.profile = new GameProfile(uuid, name);
		this.profile = changeGameProfileSkin(this.profile);

		this.manager = new PlayerInteractManager(world);
		this.player = new EntityPlayer(this.server, this.world, this.profile, this.manager);
		this.player.ping = this.ping;
	}

	private GameProfile changeGameProfileSkin(GameProfile profile) {
		String signature = "UCrkYZWG5hg/WPPE3NplBzcSNZ3BDeyQzgr6KvwZ7fVuIcdbcXH7Sb/JPVcpUq9h6EXaIAo/Q1Gb1pHvdZhj6IK/zr7lWHXd7Di22tDLgrpvE+xR7M/NVMyrOgjuLvgnrEeWUtmuzjsCB1edxX8Wu5ELrKYzF/eEVDEdaUrNefcqiBrHOzF3cJNAGmA0Ey9bRXi4t1TF8AE8bCr03iYgOkDat9854YA60/0RBD8JFJj/Odk0s/bc3059J1ZJr3DjOm7GGccmveSMVSl8/n24GtPVJPTqrqrONo8sQISBVcOF8bOLk1bkjOPt5w+v7K+3e0OgyzQQJD4ltuwCs2opFtW2CFs1LyAJU+gJVYe96o+1r6/G0GU5zrscsZcSQ9gCMzhyEzionNl+7CuybpphMlxvgX7rRX/5vv2YtlanznXEY+N1EH4o/qQ0/VO0CQFVtiOapwkaWCKtkc8u+q2JKrx65HeZ8jcz6ea1rQ1KzCKrPrbHoNtuggkNkpailKzsgYlSu35/0eDMMGQRYjJvTs+gph41uX+lZ/PVR672kBicG9jz8KrfaQf4/nbnnYCroN0EEs5tPKWg5hIlJFvidyEc8OQGjtJRn+7rNP9EilfSRjEBFUYv5VcEW7NR6dE8FGCHehDf/RUvHQOi1RwREA6zMdAtQ17unEyABMCOiAM=";
		String texture = "eyJ0aW1lc3RhbXAiOjE1MDE5NDY1MjIzNjgsInByb2ZpbGVJZCI6IjZiMjIwMzdkYzA0MzQyNzE5NGYyYWRiMDAzNjhiZjE2IiwicHJvZmlsZU5hbWUiOiJiYW5hbmFzcXVhZCIsInNpZ25hdHVyZVJlcXVpcmVkIjp0cnVlLCJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvZjJmMDg1YzZiM2NiMjI4ZTViYTgxZGY1NjJjNDc4Njc2MmYzYzI1NzEyN2U5NzI1Yzc3YjdmZDMwMWQzNyJ9fX0=";
		profile.getProperties().put("textures", new Property("textures", texture, signature));
		return profile;
	}

	public PlayerBuilder withName(String name) {
		this.profile = new GameProfile(this.profile.getId(), name);
		return this;
	}

	public PlayerBuilder withUniqueId(UUID id) {
		this.uuid = id;
		this.profile = new GameProfile(uuid, this.profile.getName());
		return this;
	}

	public PlayerBuilder withPing(int ping) {
		this.ping = ping;
		return this;
	}

	public String getAuthServerBaseUrl() {
		return "https://sessionserver.mojang.com/session/minecraft/profile/";
	}

	public void sendPacket(Player p) {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, player);
		PlayerConnection connection = ((CraftPlayer) p).getHandle().playerConnection;
		connection.sendPacket(packet);
	}

	public void sendPacketToAll() {
		PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, player);

		for (Player online : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
		}
	}

	public void removePacketToAll() {
		PacketPlayOutEntityDestroy entitypacket = new PacketPlayOutEntityDestroy(player.getId());
		PacketPlayOutPlayerInfo infopacket = new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.REMOVE_PLAYER, player);
		for (Player online : Bukkit.getOnlinePlayers()) {
			((CraftPlayer) online).getHandle().playerConnection.sendPacket(infopacket);
			((CraftPlayer) online).getHandle().playerConnection.sendPacket(entitypacket);
		}
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	public void setPlayer(EntityPlayer player) {
		this.player = player;
	}
}
