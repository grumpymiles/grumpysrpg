package com.grumpy.cosmetics.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import net.minecraft.server.v1_12_R1.NBTTagCompound;
import net.minecraft.server.v1_12_R1.NBTTagList;

public class ItemUtil {

	
	public static ItemStack createItem(Material mat, String displayName, List<String> lore)
	{
		ItemStack item = new ItemStack(mat, 1);
		
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		if(lore != null) {
			List<String> finalLore = new ArrayList<>();
            for (String s : lore)
                finalLore.add(s);
            meta.setLore(finalLore);
		}
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createItem(Material mat, String displayName, List<String> lore, int amount, byte data)
	{
		ItemStack item = new ItemStack(mat, amount, data);
		
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		if(lore != null) {
			List<String> finalLore = new ArrayList<>();
            for (String s : lore)
                finalLore.add(s);
            meta.setLore(finalLore);
		}
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createItem(Material mat, String displayName)
	{
		return createItem(mat, displayName, null);
	}
	
	public static ItemStack createItem(Material mat,  String displayName, int amount)
	{
		return createItem(mat, displayName, null, amount, (byte)0);
	}
	
	public static ItemStack createItem(Material mat,  String displayName, int amount, byte data)
	{
		return createItem(mat, displayName, null, amount, data);
	}
	
	
	
	/*  https://gist.github.com/graywolf336/8153678  */
	
	public static String[] playerInventoryToBase64(PlayerInventory inventory) throws IllegalStateException {
    	String content = toBase64(inventory);
    	String armor = itemStackArrayToBase64(inventory.getArmorContents());
    	
    	return new String[] { content, armor };
    }
	
	public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
    	try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            
            // Write the size of the inventory
            dataOutput.writeInt(items.length);
            
            // Save every element in the list
            for (int i = 0; i < items.length; i++) {
                dataOutput.writeObject(items[i]);
            }
            
            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }
	
	public static String toBase64(Inventory inventory) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            
            // Write the size of the inventory
            dataOutput.writeInt(inventory.getSize());
            
            // Save every element in the list
            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }
            
            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }
	
    public static Inventory fromBase64(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());
    
            // Read the serialized inventory
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }
            
            dataInput.close();
            return inventory;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }
    
    
    public static ItemStack[] itemStackArrayFromBase64(String data) throws IOException {
    	try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] items = new ItemStack[dataInput.readInt()];
    
            // Read the serialized inventory
            for (int i = 0; i < items.length; i++) {
            	items[i] = (ItemStack) dataInput.readObject();
            }
            
            dataInput.close();
            return items;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }
    
    public static ItemStack removeAttributes(ItemStack i){
        if(i == null || i.getType() == Material.BOOK_AND_QUILL)
             return i;
    
        ItemStack item = i.clone();
    
        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag;
        if (!nmsStack.hasTag()){
             tag = new NBTTagCompound();
             nmsStack.setTag(tag);
        }else
             tag = nmsStack.getTag();
    
        NBTTagList am = new NBTTagList();
    
        tag.set("AttributeModifiers", am);
        nmsStack.setTag(tag);
    
        return CraftItemStack.asBukkitCopy(nmsStack);
   }
}
