package com.grumpy.cosmetics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import com.grumpy.cosmetics.command.CommandCreateItem;
import com.grumpy.cosmetics.command.CommandCreateMob;
import com.grumpy.cosmetics.command.CommandCreateSpawner;
import com.grumpy.cosmetics.command.CommandModifyItem;
import com.grumpy.cosmetics.command.CommandModifyMob;
import com.grumpy.cosmetics.command.CommandModifySpawner;
import com.grumpy.cosmetics.command.CommandRankSet;
import com.grumpy.cosmetics.command.CommandRemoveMob;
import com.grumpy.cosmetics.command.CommandRemoveSpawner;
import com.grumpy.cosmetics.command.CommandSpawnerNear;
import com.grumpy.cosmetics.customentities.EntityTypes;
import com.grumpy.cosmetics.data.PlayerData;
import com.grumpy.cosmetics.data.SpawnerData;
import com.grumpy.cosmetics.listener.MenuListener;
import com.grumpy.cosmetics.listener.PlayerListener;
import com.grumpy.cosmetics.listener.WorldListener;
import com.grumpy.cosmetics.particles.ParticleEffect;
import com.grumpy.cosmetics.particles.ParticleEffectCloud;
import com.grumpy.cosmetics.particles.ParticleEffectDemonWings;
import com.grumpy.cosmetics.util.CustomConfig;
import com.grumpy.cosmetics.util.PlayerBuilder;

public class Cosmetic extends JavaPlugin {

	public static ArrayList<Entity> noFallDamageEntities = new ArrayList<>();

	public static List<PlayerData> dataPlayers = new ArrayList<>();
	public static List<ParticleEffect> particleEffectList = new ArrayList<>();

	public List<SpawnerData> spawnerList = new ArrayList<>();

	public static List<PlayerBuilder> fakeplayers = new ArrayList<>();

	CustomConfig config;

	public static boolean firstPlayer = true;

	public static Connection connection; // mysql connection

	public static boolean mysqlenable = true;

	@Override
	public void onEnable() {
		if (mysqlenable)
			setupMySQL();

		config = new CustomConfig(this, "mobs.yml", this);

		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);
		getServer().getPluginManager().registerEvents(new MenuListener(this), this);
		getServer().getPluginManager().registerEvents(new WorldListener(this), this);
		getServer().getPluginManager().registerEvents(new RankManager(), this);

		this.getCommand("ninjarankset").setExecutor(new CommandRankSet());
		this.getCommand("createmob").setExecutor(new CommandCreateMob(this));
		this.getCommand("createspawner").setExecutor(new CommandCreateSpawner(this));
		this.getCommand("removemob").setExecutor(new CommandRemoveMob(this));
		this.getCommand("removespawner").setExecutor(new CommandRemoveSpawner(this));
		this.getCommand("modifymob").setExecutor(new CommandModifyMob(this));
		this.getCommand("modifyspawner").setExecutor(new CommandModifySpawner(this));
		this.getCommand("createitem").setExecutor(new CommandCreateItem(this));
		this.getCommand("modifyitem").setExecutor(new CommandModifyItem(this));
		this.getCommand("spawnernear").setExecutor(new CommandSpawnerNear(this));
		
		// Some works without this
		Bukkit.getPluginCommand("createitem").setTabCompleter(new CommandCreateItem(this));
		//Bukkit.getPluginCommand("modifyitem").setTabCompleter(new CommandModifyItem(this));
		
		EntityTypes.registerEntities();

		particleEffectList.add(new ParticleEffectCloud(null));
		particleEffectList.add(new ParticleEffectDemonWings(null));

		refreshSpawners();

		//for /reload
		for (Player p : Bukkit.getOnlinePlayers()) {
			dataPlayers.add(new PlayerData(p.getUniqueId()));
			getPlayerData(p).setPlayer(p);
		}
			
		final BukkitRunnable countdownRunnable = new BukkitRunnable() {
			@Override
			public void run() {
				try {
					Iterator<Entity> iter = noFallDamageEntities.iterator();
					while (iter.hasNext()) {
						Entity ent = iter.next();
						if (ent.isOnGround())
							iter.remove();
					}
					Iterator<PlayerData> customPlayerIterator = dataPlayers.iterator();
					while (iter.hasNext()) {
						PlayerData customPlayer = customPlayerIterator.next();
						if (customPlayer.getPlayer() == null)
							customPlayerIterator.remove();
					}
				} catch (Exception exc) {
				}

				for (Entity entity : Bukkit.getWorld("world").getEntities()) {
					if (!entity.isInsideVehicle() && entity.getType() == EntityType.SLIME
							&& entity.getCustomName() != null && entity.getCustomName().equals("n")
							&& !entity.getPassengers().isEmpty()) {
						entity.getPassengers().get(0).remove();
						entity.remove();
					}
				}

			}
		};
		countdownRunnable.runTaskTimerAsynchronously(this, 0, 1);

		BukkitScheduler scheduler = getServer().getScheduler();
		scheduler.scheduleSyncRepeatingTask(this, new Runnable() {
			int i = 0;

			@Override
			public void run() {
				// Spawners Below
				i++;
				for (SpawnerData spawner : spawnerList) {
					spawner.used = false;
				}

				if (!Bukkit.getOnlinePlayers().isEmpty()) {
					for (Player player : Bukkit.getOnlinePlayers()) {
						Location loc = player.getLocation();
						for (SpawnerData spawner : spawnerList) {
							
							if (spawner.getLocation().distance(loc) < 45) {
								spawner.used = true;
							} else {
								if(!spawner.mobs.isEmpty())
									for(int i = 0; i < spawner.mobs.size(); i++)
									{
										for (World world : Bukkit.getWorlds()) {
									        for (Entity entity : world.getEntities())  // Should get a better solution instead of iterating over every entity
									            if (entity.getUniqueId().equals(spawner.mobs.get(i)))
									            	entity.remove();
										}
									}
								
							}
						}
					}
				} else {
					for(SpawnerData spawners : spawnerList)
					{
						spawners.mobs.clear();
					}
					for(Entity mob : Bukkit.getWorlds().get(0).getEntities())
					{
						mob.remove();
					}
				}

				if (i == 3) // 9 seconds
				{
					i = 0;
					for (SpawnerData spawner : spawnerList) {

						if (spawner.used)
							spawner.update(); // update all spawners that are enabled
					}
				}
			}
		}, 0L, 20L * 3);
	}

	public void refreshSpawners() {
		if (!spawnerList.isEmpty()) {
			for (int i = 0; i < spawnerList.size(); i++) {
				spawnerList.remove(i);
			}
		}
		if(config.contains("spawners"))
		{
			for (String section : config.getConfigurationSection("spawners").getKeys(false)) {
				spawnerList.add(config.getSpawnerData("spawners." + section, this));
			}			
		} //TODO send warning to console because there are no spawners
		
		for(Entity mob : Bukkit.getWorlds().get(0).getEntities())
		{
			if(mob.getType() != EntityType.PLAYER)
				mob.remove();
		}
	}

	@Override
	public void onDisable() {
		firstPlayer = true;
		fakeplayers.clear();
		for(Entity mob : Bukkit.getWorlds().get(0).getEntities())
		{
			if(mob.getType() != EntityType.PLAYER)
				mob.remove();
		}
	}

	public static PlayerData getPlayerData(Player player) {
		for (PlayerData playerdata : dataPlayers)
			if (playerdata.getPlayer().getUniqueId().equals(player.getUniqueId()))
				return playerdata;
		return new PlayerData(player.getUniqueId());
	}

	public void setupMySQL() {
		try {
			if (connection != null && !connection.isClosed()) {
				return;
			}

			synchronized (this) {
				if (connection != null && !connection.isClosed()) {
					return;
				}
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://" + "127.0.0.1" + ":" + 3306 + "/" + "maindata"
						+ "?autoReconnect=true&useSSL=false", "root", "");
				Bukkit.getServer().getConsoleSender().sendMessage("Main Plugin Connected to SQL database!");
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public CustomConfig getMobConfig() {
		return config;
	}

	public static Plugin getPlugin() {
		return Bukkit.getPluginManager().getPlugin("Cosmetic"); // get plugin through name
	}
}