package com.grumpy.cosmetics.listener;

import java.util.Random;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.customentities.CustomEntity;
import com.grumpy.cosmetics.data.MobData;
import com.grumpy.cosmetics.data.SpawnerData;
import com.grumpy.cosmetics.util.CustomConfig;
import com.grumpy.cosmetics.util.ItemUtil;

public class WorldListener implements Listener 
{
	Cosmetic instance;
	
	public WorldListener(Cosmetic instance)
	{
		this.instance = instance;
	}
	
	@EventHandler
	public void onEntityCombustEvent(EntityCombustEvent event)
	{
		if(event.getEntityType() == EntityType.ZOMBIE)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
    public void onBlockIgnite(BlockIgniteEvent event) {
        if (event.getCause() != IgniteCause.FLINT_AND_STEEL) {
            event.setCancelled(true);
        }
    }
	
	@EventHandler
	public void onEntityExplodeEvent(EntityExplodeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDamageEvent(EntityDamageEvent event) {
		if(event.getEntityType() == EntityType.SLIME && !event.getEntity().getCustomName().isEmpty() && event.getEntity().getCustomName().equals("n")) 
			event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onBlockFireSpread(BlockSpreadEvent event) {
		if(event.getBlock().getType() == Material.FIRE)
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onSnowForm(EntityBlockFormEvent e)
	{
		if(e.getEntity() instanceof Snowman)
			if(e.getNewState().getType() == Material.SNOW)
				e.setCancelled(true);
	}
	
	@EventHandler
	public void onEntityDieEvent(EntityDeathEvent event) throws EventException {
		Entity entity = event.getEntity();
		if(entity.getPassengers().get(0) != null && entity.getPassengers().get(0).getCustomName().equals("n") && entity.getPassengers().get(0).getPassengers().get(0) != null && entity.getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getCustomName().equals("n") && entity.getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getPassengers().get(0) != null)
		{
			event.setDroppedExp(0);
			event.getDrops().clear();
			String name = CustomConfig.getMobConfigName(entity.getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getCustomName()).replaceAll(" ", "-");
			if(instance.getMobConfig().contains("mobs." + name + ".drop"))
			{
				event.getEntity().getLocation().getWorld().dropItemNaturally(event.getEntity().getLocation(), instance.getMobConfig().getItemData("item." + instance.getMobConfig().getString("mobs." + name + ".drop")).getItemStack()); //100% drop rate
			}
			
			Random rand = new Random();

	        int n = rand.nextInt(3) + 0;
	        if(n != 0) {
	        	event.getEntity().getLocation().getWorld().dropItemNaturally(event.getEntity().getLocation(), (ItemUtil.createItem(Material.EMERALD, ChatColor.GREEN + "Emerald", n)));
	        }
			
			//remove
			entity.getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).remove();
			entity.getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).remove();
			entity.getPassengers().get(0).getPassengers().get(0).remove();
			entity.getPassengers().get(0).remove();
			UUID toRemove = UUID.fromString("11111111-1111-1111-1111-111111111111");
			for (SpawnerData spawner : instance.spawnerList) {
				if(spawner.mobs != null) {
					for(UUID uuid : spawner.mobs)
					{
						 if(entity.getUniqueId().equals(uuid)) 
						 {
						 	
						 	toRemove = uuid;
						 	break;
						 }
					}
					if(toRemove.toString() != "11111111-1111-1111-1111-111111111111") 
						spawner.mobs.remove(toRemove); //outside iteration to avoid concurrentmodificationexception
				}
			}
		}
	}
	
	@EventHandler
	public void onEntityHitEntity(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Player)
		{
			if(event.getEntity() instanceof LivingEntity && ((LivingEntity)event.getEntity()).getHealth() - event.getDamage() <= 0) //If entity will die
			{
				for (SpawnerData spawner : instance.spawnerList) {
					if(spawner.mobs != null) {
						for(UUID uuid : spawner.mobs)
						{
							 if(event.getEntity().getUniqueId().equals(uuid)) 
							 {
								 Random r = new Random();
								 int xp = 0;
								 MobData mob = spawner.getMobData();
								 if(mob.getLevel() <= 5) xp = (r.nextInt(25) + 13) + mob.getLevel()*2;
								 if(mob.getLevel() > 5) xp = mob.getLevel()*7;;
								 int leveldiff = Math.abs(mob.getLevel() - ((Player)event.getDamager()).getLevel());
								 double xppenalty = 1-(leveldiff*0.05);
								 xp *= xppenalty;
								 if(spawner.getMobData().isPassive()) xp/=5; // Prevent people from farming passive mobs
								 Cosmetic.getPlayerData(((Player) event.getDamager())).addExp(xp);
								 CustomEntity.spawnText(event.getEntity().getLocation().add(0, 1, 0), ChatColor.GRAY + "+" + xp + " xp", 20*2, instance);
							 }
						}
					}
				}
			}
		} else if(event.getDamager() instanceof Snowball)
		{
			if(((Snowball)event.getDamager()).getShooter() != null)
			{
				if(event.getEntity() instanceof Player) {
					Entity snowmanname = ((Entity) ((Snowball)event.getDamager()).getShooter()).getPassengers().get(0).getPassengers().get(0).getPassengers().get(0).getPassengers().get(0);
					if(snowmanname.getCustomName() != null)
					{
						String name = CustomConfig.getMobConfigName(snowmanname.getCustomName());
						((LivingEntity)event.getEntity()).damage(instance.getMobConfig().getMobData("mobs." + name).getMeeleDamage());
					}					
				} else if(event.getEntityType() == EntityType.SNOWMAN || event.getEntityType() == EntityType.ARMOR_STAND || event.getEntityType() == EntityType.SLIME) //snowballs from snowmans will pass through slimes, armorstands and other snowmans except for players.
				{
					event.setCancelled(true);
					event.getDamager().remove();;
					Snowball snowball = event.getDamager().getWorld().spawn(event.getDamager().getLocation().add((event.getDamager().getVelocity().normalize()).multiply(2)), Snowball.class);
							
					snowball.setVelocity(event.getDamager().getVelocity());
	                snowball.setShooter(((Snowball)event.getDamager()).getShooter());
				} 
			}
		}
	}
	@EventHandler
	public void onBlockGrowEvent(BlockGrowEvent event)
	{
		event.setCancelled(true); // does not cancel grass growth. It cancels wheat, cactus, watermelon grow etc
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event)
	{
		if(event.toWeatherState())
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockSpreadEvent(BlockSpreadEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBurn(BlockBurnEvent event)
	{
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onLeaveDecay(LeavesDecayEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockFade(BlockFadeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockFormEvent(BlockFormEvent event)
	{
		event.setCancelled(true); //Obsidian, snow form storm, etc
	}
	
	@EventHandler(priority = EventPriority.HIGH)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if(!(event.getSpawnReason() == SpawnReason.CUSTOM || event.getSpawnReason() == SpawnReason.SPAWNER_EGG)) {
           event.setCancelled(true);
        }
    }
}
