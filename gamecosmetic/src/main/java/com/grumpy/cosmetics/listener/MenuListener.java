package com.grumpy.cosmetics.listener;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.DataBaseManager;
import com.grumpy.cosmetics.TabListManager;
import com.grumpy.cosmetics.data.PlayerData;
import com.grumpy.cosmetics.data.PlayerData.Classes;
import com.grumpy.cosmetics.data.SpawnerData;
import com.grumpy.cosmetics.particles.ParticleEffect;
import com.grumpy.cosmetics.particles.ParticleEffectType;
import com.grumpy.cosmetics.util.ItemUtil;

public class MenuListener implements Listener {
	
	Cosmetic instance;
	
	public MenuListener(Cosmetic instance)
	{
		this.instance = instance;
	}
	
	//Class selection
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event)
	{
		event.getPlayer().setResourcePack("https://download.nodecdn.net/containers/nodecraft/minepack/cb889a29a7074a7a692ef1681d8457bf.zip");
		Cosmetic.dataPlayers.add(new PlayerData(event.getPlayer().getUniqueId()));
		Player player = event.getPlayer();
		player.getInventory().setHelmet(null);
		player.getInventory().setChestplate(null);
		player.getInventory().setLeggings(null);
		player.getInventory().setBoots(null);
		player.getInventory().clear();
		player.setHealth(20);
		player.setFoodLevel(20);
		player.setLevel(0);
		player.setExp(0);
		player.teleport(new Location(player.getWorld(), 0, 100, 0)); // Hard coded spawn location
		if(Bukkit.getOnlinePlayers().size() == 1)
		{
			for(SpawnerData spawners : instance.spawnerList)
			{
				spawners.mobs.clear();
			}
			for(Entity mob : Bukkit.getWorlds().get(0).getEntities())
			{
				mob.remove();
			}
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(Cosmetic.getPlugin(), new Runnable() {
		    public void run() {
		    		openClassSlotSelectionMenu(player);		    	
		    }
		}, 20L);
	}
	
	@EventHandler
	public void onPlayerCloseInventory(InventoryCloseEvent event) {
		if(event.getPlayer() != null) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(Cosmetic.getPlugin(), new Runnable() { // Without delayed task this loops through and prints a wall of errors
			    public void run() {
			    	Player player = (Player) event.getPlayer();
			    	if(Bukkit.getOnlinePlayers().contains(player))
			    	{
			    		if(Cosmetic.getPlayerData(player).isInClassSelection() && !player.getOpenInventory().getTopInventory().getTitle().contains("Class Selection"))
			    			openClassSlotSelectionMenu(player);   				    		
			    	}
			    }
			}, 20L);
		}
	}
	
	private static void openClassSelectionMenu(Player player)
	{
		Inventory inv = Bukkit.createInventory(null, 9, "Class Selection");
		
		inv.setItem(1, ItemUtil.createItem(Material.IRON_SWORD, ChatColor.GOLD + "Warrior"));
		inv.setItem(3, ItemUtil.createItem(Material.BLAZE_ROD, ChatColor.GOLD + "Sorcerer"));
		inv.setItem(5, ItemUtil.createItem(Material.BOW, ChatColor.GOLD + "Archer"));
		inv.setItem(7, ItemUtil.createItem(Material.IRON_SPADE, ChatColor.GOLD + "Spearman"));
		
		player.openInventory(inv);
	}
	
	private static void openClassSlotSelectionMenu(Player player)
	{
		if (Cosmetic.mysqlenable) {
			if(!DataBaseManager.playerDataExists(player.getUniqueId()))
			{
				DataBaseManager.createPlayerData(player.getUniqueId());
				Inventory inv = Bukkit.createInventory(null, 9, "Select Class");
				for(int i = 1; i < 5; i++)
				{
					inv.setItem(i+i-1, ItemUtil.createItem(Material.WOOL, ChatColor.GREEN + "Empty Slot " + i, 1, (byte)13));
					
				}
				player.openInventory(inv);
					
			} else {
				Inventory inv = Bukkit.createInventory(null, 9, "Select Class");
				
				for(int i = 1; i < 5; i++) // 4 times
				{
					if(!DataBaseManager.getSlot(player.getUniqueId(), i).equals(""))
					{
					Material mat;
					String className = DataBaseManager.getSlot(player.getUniqueId(), i).substring(0, 1).toUpperCase(); //first character
					className += DataBaseManager.getSlot(player.getUniqueId(), i).split(";")[0].substring(1).toLowerCase(); //get rest of characters
					switch(className.toUpperCase()) {
					case "WARRIOR":
						mat = Material.IRON_SWORD;
						break;
					case "SORCERER":
						mat = Material.BLAZE_ROD;
						break;
					case "ARCHER":
						mat = Material.BOW;
						break;
					case "SPEARMAN":
						mat = Material.IRON_SPADE;
						break;
					default:
						mat = Material.AIR;
					}
					inv.setItem(i+i-1, ItemUtil.createItem(mat, ChatColor.RED + "Select " + className, Arrays.asList(ChatColor.GOLD + "Lvl " + DataBaseManager.getSlot(player.getUniqueId(), i).split(";")[4])));
					} else {
						inv.setItem(i+i-1, ItemUtil.createItem(Material.WOOL, ChatColor.GREEN + "Empty Slot " + i, 1, (byte)13));
					}				
				}
				player.openInventory(inv);
				
				
			}	
		} else {
			player.sendMessage(ChatColor.DARK_RED + "Mysql is not enabled! Please Contact an Administrator!");
		}
	}
	
	@EventHandler
	public void onClassSelectionClick(InventoryClickEvent event) {
		if (event.getInventory().getTitle().contains("Class Selection") || event.getInventory().getTitle().contains("Select Class")) {
			event.setCancelled(true);
			
			if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta())
				return;
			if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
				if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Empty Slot") && event.getCurrentItem().getType() == Material.WOOL)
				{
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot = Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName().substring(13)); // "ColorcodeEmpty Slot Integer"
					openClassSelectionMenu((Player) event.getWhoClicked());
				} else if(event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Warrior"))
				{
					DataBaseManager.setSlot(event.getWhoClicked().getUniqueId(), Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot, "WARRIOR;");
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).setSlotClass(Classes.WARRIOR, Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot);
					openClassSlotSelectionMenu((Player) event.getWhoClicked());
				} else if(event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Sorcerer"))
				{
					DataBaseManager.setSlot(event.getWhoClicked().getUniqueId(), Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot, "SORCERER;");
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).setSlotClass(Classes.SORCERER, Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot);
					openClassSlotSelectionMenu((Player) event.getWhoClicked());
				} else if(event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Archer"))
				{
					DataBaseManager.setSlot(event.getWhoClicked().getUniqueId(), Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot, "ARCHER;");
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).setSlotClass(Classes.ARCHER, Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot);
					openClassSlotSelectionMenu((Player) event.getWhoClicked());
				
				} else if(event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.GOLD + "Spearman"))
				{
					DataBaseManager.setSlot(event.getWhoClicked().getUniqueId(), Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot, "SPEARMAN;");
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).setSlotClass(Classes.SPEARMAN, Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot);
					openClassSlotSelectionMenu((Player) event.getWhoClicked());
				} else if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Select"))
				{
					Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentPlayerSlot = (event.getSlot()+1)/2;
					Player player = (Player) event.getWhoClicked();
					player.playSound(player.getLocation(), Sound.BLOCK_NOTE_PLING, 10.0f, 1.0f);
					playerJoinGame(player);
				}
			}
		}
	}
	
	public void playerJoinGame(Player player)
	{
		Cosmetic.getPlayerData(player).setClassSelection(false);
		if(!player.getInventory().contains(ItemUtil.createItem(Material.CHEST, ChatColor.GREEN + "Cosmetics"))) {
			player.getInventory().setItem(8, ItemUtil.createItem(Material.CHEST, ChatColor.GREEN + "Cosmetics")); //add the item if player doesnt have one
		}
		
		player.setLevel(1);
		player.teleport(new Location(player.getWorld(), 24.5, 48, 188.5, 180, 0)); // only does something when it is the player's first time
		if(DataBaseManager.getSlot(player.getUniqueId(), Cosmetic.getPlayerData(player).currentPlayerSlot).split(";")[0].equals("WARRIOR")) player.getInventory().addItem(instance.getMobConfig().getItemData("item." + "Rookie's-Sword").getItemStack());
		if(DataBaseManager.getSlot(player.getUniqueId(), Cosmetic.getPlayerData(player).currentPlayerSlot).split(";")[0].equals("SORCERER")) player.getInventory().addItem(instance.getMobConfig().getItemData("item." + "Rookie's-Wand").getItemStack());
		if(DataBaseManager.getSlot(player.getUniqueId(), Cosmetic.getPlayerData(player).currentPlayerSlot).split(";")[0].equals("ARCHER")) player.getInventory().addItem(instance.getMobConfig().getItemData("item." + "Rookie's-Bow").getItemStack());
		if(DataBaseManager.getSlot(player.getUniqueId(), Cosmetic.getPlayerData(player).currentPlayerSlot).split(";")[0].equals("SPEARMAN")) player.getInventory().addItem(instance.getMobConfig().getItemData("item." + "Rookie's-Spear").getItemStack());
		
		DataBaseManager.setPlayerFromMysql(player, Cosmetic.getPlayerData(player).currentPlayerSlot);
		Cosmetic.getPlayerData(player).setPlayer(player);
		
		if(Bukkit.getServer().getOnlinePlayers().size() == 1 && Cosmetic.fakeplayers.isEmpty())
		{
			TabListManager.firstPlayer(player, 80);
		} else {
			TabListManager.removePlayer();
		}
		TabListManager.setRealPlayersFirst();
	}
	
	
	
	
	//Particles
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if (event.getItem() != null && event.getItem().getType() == Material.CHEST && event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName() && event.getItem().getItemMeta().getDisplayName().contains("Cosmetics")) {
			event.setCancelled(true);
			openParticlesMenu(event.getPlayer());
		}
	}

	@EventHandler
	public void onParticlesClick(InventoryClickEvent event) {
		if (event.getInventory().getTitle().equals("Particles")) {
			event.setCancelled(true);
			
			if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta())
				return;
			if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
				for (ParticleEffect particleEffect : Cosmetic.particleEffectList) {
					if (event.getCurrentItem().getItemMeta().getDisplayName().equals(particleEffect.getName())) {
						if (Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentParticleEffect != null
								&& Cosmetic.getPlayerData((Player) event.getWhoClicked()).currentParticleEffect
								.getType().equals(getParticleEffectByName(
										event.getCurrentItem().getItemMeta().getDisplayName()))) {
							Cosmetic.getPlayerData((Player) event.getWhoClicked()).removeParticleEffect();
						} else {
							StringBuilder sb = new StringBuilder();
							for (int i = 1; i < event.getCurrentItem().getItemMeta().getDisplayName().split(" ").length; i++) {
								sb.append(event.getCurrentItem().getItemMeta().getDisplayName().split(" ")[i]);
								try {
									if (event.getCurrentItem().getItemMeta().getDisplayName().split(" ")[i + 1] != null)
										sb.append(" ");
								} catch (Exception exc) {
									
								}
							}
							activateParticleEffectByType(getParticleEffectByName(event.getCurrentItem().getItemMeta().getDisplayName()), (Player) event.getWhoClicked());
						}
					}
				}
			}
			
		}
	}
	
	private static void openParticlesMenu(Player player) {
		Inventory inv = Bukkit.createInventory(null, 27, "Particles");
		int i = 12;
		for (ParticleEffect particleEffect : Cosmetic.particleEffectList) {
			ItemStack item = ItemUtil.createItem(particleEffect.getMaterial(), particleEffect.getName());
			inv.setItem(i, item);
			i += 2; // second next slot for the next particle
		}

		player.openInventory(inv);
	}

	public static ParticleEffectType getParticleEffectByName(String name) {
		for (ParticleEffect particleEffect : Cosmetic.particleEffectList) {
			if (particleEffect.getName().equals(name)) {
				return particleEffect.getType();
			}
		}
		return null;
	}

	public static void activateParticleEffectByType(ParticleEffectType type, final Player player) {
		for (ParticleEffect particleEffect : Cosmetic.particleEffectList) {
			if (particleEffect.getType() == type) {
				Class particleEffectClass = particleEffect.getClass();

				Class[] cArg = new Class[1];
				cArg[0] = UUID.class;

				UUID uuid = player.getUniqueId();

				try {
					particleEffectClass.getDeclaredConstructor(UUID.class).newInstance(uuid);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
