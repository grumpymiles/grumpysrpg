package com.grumpy.cosmetics.listener;

import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.ChannelPromise;
import net.minecraft.server.v1_12_R1.PacketPlayOutSpawnEntityLiving;

public class PacketListener implements Listener {
	
	
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		injectPlayerToPacketListener(event.getPlayer());
	}
	
	@EventHandler
	public void onPlayerLeave(PlayerQuitEvent event)
	{
		removePlayer(event.getPlayer());
	}
	
	
	private void removePlayer(Player player) {
		Channel channel = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel;
		channel.eventLoop().submit(()->{
			channel.pipeline().remove(player.getName());
			return null;
		});
	}
	
	private void injectPlayerToPacketListener(Player player)
	{
		ChannelDuplexHandler channelDuplexHandler = new ChannelDuplexHandler() {
			
			@Override
			public void channelRead(ChannelHandlerContext context, Object packet) throws Exception {
				if(packet instanceof PacketPlayOutSpawnEntityLiving)
				{
					/*packet = new PacketPlayOutSpawnEntityLiving(new EntityZombie(((CraftPlayer) player).getHandle().getWorld()));
					try {
						Field entityId = packet.getClass().getDeclaredField("a");
						entityId.setAccessible(true);
						entityId.set(packet, player.getEntityId());
					} catch(Exception e) {}*/
				}
				super.channelRead(context, packet);
			}
			
			@Override
			public void write(ChannelHandlerContext context, Object packet, ChannelPromise channelPromise) throws Exception {
					super.write(context, packet, channelPromise);
			}
		};
		
		ChannelPipeline pipeline = ((CraftPlayer) player).getHandle().playerConnection.networkManager.channel.pipeline();
		pipeline.addBefore("packet_handler", player.getName(), channelDuplexHandler);
	}
}
