package com.grumpy.cosmetics.listener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.DataBaseManager;
import com.grumpy.cosmetics.TabListManager;
import com.grumpy.cosmetics.customentities.CustomEntity;
import com.grumpy.cosmetics.data.ItemData;
import com.grumpy.cosmetics.util.Particles;
import com.grumpy.cosmetics.util.UtilParticles;

public class PlayerListener implements Listener {
	
	Cosmetic instance;
	
	public PlayerListener(Cosmetic instance) {
		this.instance = instance;
	}
	
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event) {
        if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.CHEST && event.getCurrentItem().getItemMeta().hasDisplayName() && event.getCurrentItem().getItemMeta().getDisplayName().contains("Cosmetics")) {
            event.setCancelled(true);
        }
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onCommandPreprocess(PlayerCommandPreprocessEvent e) {
		
        if (e.getMessage().toLowerCase().startsWith("/ping ")||e.getMessage().equalsIgnoreCase("/ping")) {
        	e.getPlayer().sendMessage(ChatColor.GOLD + "Your ping is: " + ChatColor.GREEN + ((CraftPlayer) e.getPlayer()).getHandle().ping + "ms");
        	CustomEntity.spawnText(e.getPlayer().getEyeLocation(), "Test", 20*4, instance);
            e.setCancelled(true);
        }
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event)
	{
		DataBaseManager.savePlayerToMysql(event.getPlayer(), Cosmetic.getPlayerData(event.getPlayer()).currentPlayerSlot);
		if(!Bukkit.getOnlinePlayers().isEmpty())
		{
			TabListManager.addPlayer();
		} else {
			Cosmetic.fakeplayers.clear(); // save memory when no one is online **Might be bad idea because we have a for loop to create the players if this is null**

		}
		Cosmetic.dataPlayers.remove(Cosmetic.getPlayerData(event.getPlayer()));
	}
	
	@EventHandler
	public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
		if(event.getRightClicked() instanceof Villager)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		if(event.getPlayer().getInventory().getItemInMainHand() != null && event.getPlayer().getInventory().getItemInMainHand().hasItemMeta() && event.getPlayer().getInventory().getItemInMainHand().getItemMeta().hasDisplayName() && instance.getMobConfig().contains("item." + ChatColor.stripColor(event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().replaceAll(" ", "-")))) {
			ItemData item = instance.getMobConfig().getItemData("item." + ChatColor.stripColor(event.getPlayer().getInventory().getItemInMainHand().getItemMeta().getDisplayName().replaceAll(" ", "-")));
			if(Cosmetic.getPlayerData(event.getPlayer()).getCurrentClass() != item.getWeaponClass())
			{
				if(event.getPlayer().getLevel() >= item.getLevel())
				{
					if(event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK)
					{
						//TODO main attack					
					}					
				} else {
					event.getPlayer().sendMessage(ChatColor.DARK_RED + "You need to be atleast level " + ChatColor.RED + item.getLevel() + ChatColor.DARK_RED + " to use this weapon!");
					event.setCancelled(true);
				}
			} else {
				event.getPlayer().sendMessage(ChatColor.RED + item.getName().substring(0, 1) + item.getName().substring(1).toLowerCase().replaceAll("-", "") + ChatColor.DARK_RED + " is for " + ChatColor.RED + item.getWeaponClass().toString().substring(0, 1) + item.getWeaponClass().toString().substring(1).toLowerCase() + ChatColor.DARK_RED + "s only!");
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerDie(EntityDamageEvent event)
	{
		if(event.getEntity() instanceof Player)
		{
			if(((LivingEntity)event.getEntity()).getHealth() - event.getDamage() <= 0)
			{
				
				event.setCancelled(true);
				((LivingEntity)event.getEntity()).setHealth(((LivingEntity)event.getEntity()).getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
				((Player) event.getEntity()).sendMessage(ChatColor.RED + "You died!");
				((Player) event.getEntity()).teleport(new Location(event.getEntity().getWorld(), 24.5, 48, 188.5, 180, 0));
				UtilParticles.display(Particles.REDSTONE, 0.5F, 0.1f, 0.5f, event.getEntity().getLocation().add(0, 1, 0), 10, 5);
				((Player) event.getEntity()).playSound(event.getEntity().getLocation(), Sound.ENTITY_GHAST_SCREAM, 1, 1);
				((Player) event.getEntity()).setFoodLevel(20);
			}
		}
	}
}