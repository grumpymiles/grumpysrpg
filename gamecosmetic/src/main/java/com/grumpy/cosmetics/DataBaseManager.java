package com.grumpy.cosmetics;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.grumpy.cosmetics.data.PlayerData.Classes;
import com.grumpy.cosmetics.util.ItemUtil;

public class DataBaseManager {
	
	//Player data
	
	public static void savePlayerToMysql(Player player, int slot) // modifies the database
	{
		List<String> playerString = new ArrayList<String>();
		
		if(playerString.size() >= 5 && playerString.get(1).contains("Loc"))
		{
			String[] playerInventory = ItemUtil.playerInventoryToBase64(player.getInventory());
			playerString.add(Cosmetic.getPlayerData(player).getSlotClass(Cosmetic.getPlayerData(player).currentPlayerSlot).toString());
			playerString.add(";Loc" + player.getLocation().getBlockX() + "Q" + player.getLocation().getBlockY() + "Q" + player.getLocation().getBlockZ() + "Q" + (int) player.getLocation().getYaw() + "Q" + (int) player.getLocation().getPitch());
			playerString.add(";" + playerInventory[0]);
			playerString.add(";" + playerInventory[1]);
			playerString.add(";" + player.getLevel());
			playerString.add(";" + player.getExp());
		} else  {
			String[] playerInventory = ItemUtil.playerInventoryToBase64(player.getInventory());
			playerString.add(Cosmetic.getPlayerData(player).getSlotClass(Cosmetic.getPlayerData(player).currentPlayerSlot).toString());
			playerString.add(1, ";Loc" + player.getLocation().getBlockX() + "Q" + player.getLocation().getBlockY() + "Q" + player.getLocation().getBlockZ() + "Q" + (int) player.getLocation().getYaw() + "Q" + (int) player.getLocation().getPitch());
			playerString.add(";" + playerInventory[0]);
			playerString.add(";" + playerInventory[1]);
			playerString.add(";" + player.getLevel());
			playerString.add(";" + player.getExp());
		}
		
		String result = "";
		for(String string : playerString)
			result += string;
		DataBaseManager.setSlot(player.getUniqueId(), slot, result);
	}
	
	
	public static void setPlayerFromMysql(Player player, int slot) // Modifies the player
	{
		String[] playerString = DataBaseManager.getSlot(player.getUniqueId(), slot).split(";");
		if(playerString.length >= 5 && playerString[1].contains("Loc"))
		{
			Cosmetic.getPlayerData(player).setSlotClass(Classes.valueOf(playerString[0]), slot);
			String[] locstring = playerString[1].replaceAll("Loc", "").split("Q");
			player.teleport(new Location(player.getWorld(), Integer.parseInt(locstring[0]), Integer.parseInt(locstring[1]) + 1, Integer.parseInt(locstring[2]), Integer.parseInt(locstring[3]), Integer.parseInt(locstring[4])));
			try {
				player.getInventory().setContents(ItemUtil.itemStackArrayFromBase64(playerString[2]));
				player.getInventory().setArmorContents((ItemUtil.itemStackArrayFromBase64(playerString[3])));
			} catch (IllegalArgumentException | IOException e) {
				e.printStackTrace();
			}
			
			player.setLevel(Integer.parseInt(playerString[4]));
			player.setExp(Float.parseFloat(playerString[5]));
		}
	}
	
	@SuppressWarnings("unused")
	private static void updateData(UUID uuid, int slot) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("UPDATE " + "playerdata" + " SET SLOT" + slot + "=? WHERE UUID=?");
			statement.setInt(1, 0);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public static boolean playerDataExists(UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("SELECT * FROM " + "playerdata" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			
			ResultSet results = statement.executeQuery();
			if (results.next()) { //Player exists in database
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	
	public static boolean playerDataSlotExists(UUID uuid, int slot) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("SELECT * FROM " + "playerdata" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();

			return results.getString("SLOT" + slot) != "";

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static String getSlot(UUID uuid, int slot) {
		try {
			PreparedStatement statement = Cosmetic.connection
					.prepareStatement("SELECT * FROM " + "playerdata" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();

			return results.getString("SLOT" + slot);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null; // returns if it crashes
	}
	
	public static void createPlayerData(final UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("SELECT * FROM " + "playerdata" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();
			if (playerDataExists(uuid) != true) {
				PreparedStatement insert = Cosmetic.connection.prepareStatement("INSERT INTO " + "playerdata" + "(UUID,SLOT1,SLOT2,SLOT3,SLOT4) VALUE (?,?,?,?,?)");
				insert.setString(1, uuid.toString());
				insert.setString(2, ""); // set all the player slots to nothing
				insert.setString(3, "");
				insert.setString(4, "");
				insert.setString(5, "");
				insert.executeUpdate();
				//updateData(uuid, 1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void setSlot(UUID uuid, int slot, String value) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("UPDATE " + "playerdata" + " SET SLOT" + slot + "=? WHERE UUID=?");
			statement.setString(1, value);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void addToSlot(UUID uuid, int slot, String value) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("UPDATE " + "playerdata" + " SET SLOT" + slot + " = CONCAT(SLOT" + slot + ", '" + value + "') WHERE UUID=?");
			statement.setString(1, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	//Player rank
	public static boolean playerRankExists(UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());

			ResultSet results = statement.executeQuery();
			if (results.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public static void createPlayer(final UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();
			if (playerRankExists(uuid) != true) {
				PreparedStatement insert = Cosmetic.connection.prepareStatement("INSERT INTO " + "playerrank" + "(UUID,RANK) VALUE (?,?)");
				insert.setString(1, uuid.toString());
				insert.setInt(2, 0); // set player to default rank
				insert.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateRank(UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection
					.prepareStatement("UPDATE " + "playerrank" + " SET RANK=? WHERE UUID=?");
			statement.setInt(1, 0);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static int getRank(UUID uuid) {
		try {
			PreparedStatement statement = Cosmetic.connection
					.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();

			return results.getInt("RANK");

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0; // returns if it crashes
	}

	public static void setRank(UUID uuid, int rank) {
		try {
			PreparedStatement statement = Cosmetic.connection
					.prepareStatement("UPDATE " + "playerrank" + " SET RANK=? WHERE UUID=?");
			statement.setInt(1, rank);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


}
