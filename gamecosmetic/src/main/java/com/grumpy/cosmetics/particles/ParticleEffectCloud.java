package com.grumpy.cosmetics.particles;

import java.util.UUID;

import org.bukkit.Material;

import com.grumpy.cosmetics.util.Particles;
import com.grumpy.cosmetics.util.UtilParticles;

public class ParticleEffectCloud extends ParticleEffect {

	public ParticleEffectCloud(UUID owner){
        super(Particles.CLOUD, Material.BLAZE_ROD, "Cloud", owner, ParticleEffectType.CLOUD, 1);
    }
	
	@Override
	void onUpdate() {
		UtilParticles.display(Particles.CLOUD, 0.5F, 0.1f, 0.5f, getPlayer().getLocation().add(0, 0.1, 0), 10);
	}

}
