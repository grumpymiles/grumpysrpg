package com.grumpy.cosmetics.particles;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.util.Particles;
import com.grumpy.cosmetics.util.UtilParticles;

import net.md_5.bungee.api.ChatColor;

public abstract class ParticleEffect {
	
	private Material material;
	private String name;
	
	boolean moving;
	
	private UUID owneruuid;
	
	private Particles particle;
	int repeatdelay;
	
	private ParticleEffectType type = ParticleEffectType.DEFAULT;
	
	public ParticleEffect(final Particles particle, Material material, String configName, final UUID owneruuid, final ParticleEffectType type, int repeatdelay)
	{
		this.material = material;
		this.name = configName;
		this.particle = particle;
		this.type = type;
		this.repeatdelay = repeatdelay;
		
		if (owneruuid != null) {
			this.owneruuid = owneruuid;
			
		if(Cosmetic.getPlayerData(Bukkit.getPlayer(owneruuid)).currentParticleEffect != null)
			Cosmetic.getPlayerData(Bukkit.getPlayer(owneruuid)).removeParticleEffect();
		
		 BukkitRunnable runnable = new BukkitRunnable() {
             @Override
             public void run() {
                 if (Bukkit.getPlayer(owneruuid) != null && Cosmetic.getPlayerData(Bukkit.getPlayer(owneruuid)).currentParticleEffect != null && Cosmetic.getPlayerData(Bukkit.getPlayer(owneruuid)).currentParticleEffect.getType() == type) {
                     if(moving)
                     {
                    	 UtilParticles.display(particle, 0.4f, 0.3f, 0.4f, getPlayer().getLocation().add(0, 0.2, 0), 3);
                    	 moving = false;
                     } else {
                    	 onUpdate();                    	 
                     }
                 } else {
                     cancel();
                 }
             }
         };
         runnable.runTaskTimer(Cosmetic.getPlugin(), 0, repeatdelay);
         new ParticleEffectListener(this);
         
         Cosmetic.getPlayerData(getPlayer()).currentParticleEffect = this;
         getPlayer().sendMessage(ChatColor.GREEN + Cosmetic.getPlayerData(getPlayer()).currentParticleEffect.getName() + ChatColor.GOLD + " Has been activated");
		 }
	}
	
	public Particles getEffect() {
        return particle;
    }
	
	public String getConfigName() {
        return name;
    }
	
	public String getName() {
	    return name;
	}
	
	public Material getMaterial() {
        return this.material;
    }
	
	public ParticleEffectType getType() {
	    return type;
	}
	
	abstract void onUpdate();
	
	protected Player getPlayer() {
        return Bukkit.getPlayer(owneruuid);
    }
	
	protected UUID getOwner() {
		return owneruuid;
	}
	
	public class ParticleEffectListener implements Listener {
        private ParticleEffect particleEffect;

        public ParticleEffectListener(ParticleEffect particleEffect) {
            this.particleEffect = particleEffect;
            Cosmetic.getPlugin().getServer().getPluginManager().registerEvents(this, Cosmetic.getPlugin());
        }

        @EventHandler
        public void onMove(PlayerMoveEvent event) {
            if (getPlayer() == event.getPlayer()
                    && (event.getFrom().getX() != event.getTo().getX()
                    || event.getFrom().getY() != event.getTo().getY()
                    || event.getFrom().getZ() != event.getTo().getZ()))
                particleEffect.moving = true;
        }

    }
}
