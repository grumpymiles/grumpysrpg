package com.grumpy.cosmetics.particles;

public enum ParticleEffectType {
	
	DEFAULT(""),
	CLOUD("Cloud"),
	DEMONWINGS("DemonWings");
	
	String configName;
	
	ParticleEffectType(String configName) {
		this.configName = configName;
	}
}
