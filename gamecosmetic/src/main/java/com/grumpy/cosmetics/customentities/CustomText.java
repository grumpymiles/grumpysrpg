package com.grumpy.cosmetics.customentities;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

import com.grumpy.cosmetics.Cosmetic;

import net.minecraft.server.v1_12_R1.AxisAlignedBB;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EnumHand;
import net.minecraft.server.v1_12_R1.EnumInteractionResult;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.ItemStack;
import net.minecraft.server.v1_12_R1.Vec3D;
import net.minecraft.server.v1_12_R1.World;
import net.minecraft.server.v1_12_R1.WorldServer;

public class CustomText extends EntityArmorStand {
	
	public CustomText(World mcWorld)
	{
		super(mcWorld);
		setInvisible(true);
	}
	
	public void setName(String text) {
        super.setCustomName(text);
        super.setCustomNameVisible(!text.isEmpty());
    }
	
	@Override
    public void a(AxisAlignedBB boundingBox) {
	//	this.a(new NullBoundingBox());
    }
	
	@Override
	public EnumInteractionResult a(EntityHuman entityhuman, Vec3D vec3d, EnumHand enumhand) {
        // Prevent stand being equipped
        return EnumInteractionResult.FAIL;
    }

    @Override
    public boolean c(int i, ItemStack itemstack) {
        // Prevent stand being equipped
        return false;
    }

    @Override
    public void setSlot(EnumItemSlot enumitemslot, ItemStack itemstack) {
        // Prevent stand being equipped
    }
	    
	public static UUID spawnEntity(Location loc, String name, int ticks, Cosmetic instance)
	{
		WorldServer mcWorld = ((CraftWorld) loc.getWorld()).getHandle();
		final CustomText customEnt = new CustomText(mcWorld);
		customEnt.setSmall(true);
		customEnt.setArms(false);
		customEnt.setNoGravity(true);
		customEnt.setBasePlate(true);
		customEnt.setMarker(true);
		customEnt.getBukkitEntity().setCustomName(name);
		customEnt.getBukkitEntity().setCustomNameVisible(true);
		customEnt.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		Bukkit.getScheduler().scheduleSyncDelayedTask(instance,new Runnable(){
            @Override
            public void run() {
            	customEnt.getBukkitEntity().remove();
            }
        }, ticks);
		
		mcWorld.addEntity(customEnt, SpawnReason.CUSTOM);
		return customEnt.getUniqueID();
	}
}
