package com.grumpy.cosmetics.customentities.pathfinder;

import org.bukkit.Location;

import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.Navigation;
import net.minecraft.server.v1_12_R1.PathEntity;
import net.minecraft.server.v1_12_R1.PathfinderGoal;

public class PathfinderGoalGaurd extends PathfinderGoal {
	
	 private float speed;

	    private EntityInsentient entity;

	    private Location loc;

	    private Navigation navigation;

	    public PathfinderGoalGaurd(EntityInsentient entity, Location loc, float speed) {
	        this.entity = entity;
	        this.loc = loc;
	        this.navigation = (Navigation) this.entity.getNavigation();
	        this.speed = speed;
	    }

	    @Override
	    public boolean a() {
	        return true;
	    }

	    @Override
	    public boolean b() {
	        return false;
	    }

	    @Override
	    public void c() {
	        PathEntity pathEntity = this.navigation.a(loc.getX(), loc.getY(), loc.getZ());
	        this.navigation.a(pathEntity, speed);
	    }
}
