package com.grumpy.cosmetics.customentities;

import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.CraftWorld;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Slime;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.grumpy.cosmetics.data.MobInventoryData;

import net.minecraft.server.v1_12_R1.DamageSource;
import net.minecraft.server.v1_12_R1.EntityBlaze;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EnumItemSlot;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import net.minecraft.server.v1_12_R1.PathfinderGoalFloat;
import net.minecraft.server.v1_12_R1.PathfinderGoalLookAtPlayer;
import net.minecraft.server.v1_12_R1.PathfinderGoalMeleeAttack;
import net.minecraft.server.v1_12_R1.PathfinderGoalNearestAttackableTarget;
import net.minecraft.server.v1_12_R1.PathfinderGoalRandomLookaround;
import net.minecraft.server.v1_12_R1.PathfinderGoalRandomStroll;
import net.minecraft.server.v1_12_R1.World;
import net.minecraft.server.v1_12_R1.WorldServer;

public class CustomBlaze  extends EntityBlaze {

	public CustomBlaze(World mcWorld)
	{
		super(mcWorld);
	}
	
	@Override
	protected void r() {
		this.goalSelector.a(0, new PathfinderGoalFloat(this));
        this.goalSelector.a(2, new PathfinderGoalMeleeAttack(this, 1.0D, true));
        this.goalSelector.a(8, new PathfinderGoalRandomLookaround(this));
        this.goalSelector.a(9, new PathfinderGoalRandomStroll(this, 1.0D));
        this.goalSelector.a(10, new PathfinderGoalLookAtPlayer(this, EntityHuman.class, 10.0F));
        
        this.getAttributeInstance(GenericAttributes.MOVEMENT_SPEED).setValue(0.23);
	}
	
	public void setHostile()
	{
		this.targetSelector.a(2, new PathfinderGoalNearestAttackableTarget(this, EntityHuman.class,  true));
	}
	
	@Override
	public boolean damageEntity(DamageSource damagesource, float f) { //onentitygethurt
		
        if (super.damageEntity(damagesource, f)) {
            EntityLiving entityliving = this.getGoalTarget();
            if(this.getBukkitEntity() != null && this.getBukkitEntity().getPassenger() != null && this.getBukkitEntity().getPassenger().getCustomName().equals("n") && !this.getBukkitEntity().getPassenger().getPassengers().get(0).isCustomNameVisible())
            {
            	this.getBukkitEntity().getPassenger().getPassengers().get(0).setCustomNameVisible(true);
            }
            ArmorStand health = (ArmorStand) this.getBukkitEntity().getPassenger().getPassengers().get(0);
            health.setCustomName(ChatColor.DARK_RED + "[" + ChatColor.RED + (int)this.getHealth() + "/" + (int)this.getMaxHealth() + ChatColor.DARK_RED + "]");
            
            if (entityliving == null && damagesource.getEntity() instanceof EntityLiving) {
                entityliving = (EntityLiving) damagesource.getEntity();
            }
            return true;
        } else {
            return false;
        }
    }
	
	public static UUID spawnEntity(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		WorldServer mcWorld = ((CraftWorld) loc.getWorld()).getHandle();
		final CustomBlaze customEnt = new CustomBlaze(mcWorld);
		if(!passive) customEnt.setHostile();
		customEnt.getAttributeInstance(GenericAttributes.maxHealth).setValue(health);
		customEnt.getAttributeInstance(GenericAttributes.ATTACK_DAMAGE).setValue(meeledamage);
		customEnt.setHealth(health);
		customEnt.setLocation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
		
		if(inventory != null)
		{
			customEnt.getBukkitEntity().getHandle().setEquipment(EnumItemSlot.FEET, CraftItemStack.asNMSCopy(new ItemStack(inventory.getContents()[0])));
			customEnt.getBukkitEntity().getHandle().setEquipment(EnumItemSlot.LEGS, CraftItemStack.asNMSCopy(new ItemStack(inventory.getContents()[1])));
			customEnt.getBukkitEntity().getHandle().setEquipment(EnumItemSlot.CHEST, CraftItemStack.asNMSCopy(new ItemStack(inventory.getContents()[2])));
			customEnt.getBukkitEntity().getHandle().setEquipment(EnumItemSlot.HEAD, CraftItemStack.asNMSCopy(new ItemStack(inventory.getContents()[3])));
			customEnt.getBukkitEntity().getHandle().setEquipment(EnumItemSlot.MAINHAND, CraftItemStack.asNMSCopy(new ItemStack(inventory.getContents()[4])));				
		}
		
		ArmorStand as = loc.getWorld().spawn(loc, ArmorStand.class);
		as.setCustomName((passive ? ChatColor.GREEN : ChatColor.RED) + name.replaceAll("_", " ") + ChatColor.GOLD + " [Lvl. " + level + "]");
		as.setCustomNameVisible(true);
		as.setVisible(false);
		as.setSmall(true);
		as.setMarker(false);
		
		Slime s = loc.getWorld().spawn(loc, Slime.class);
		s.setCustomName("n");
		s.setCustomNameVisible(false);
		s.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 1));
		s.setSize(-1);
		s.addPassenger(as);
		
		ArmorStand has = loc.getWorld().spawn(loc, ArmorStand.class);
		has.setCustomName(".");
		has.setCustomNameVisible(false);
		has.setVisible(false);
		has.setSmall(true);
		has.setMarker(false);
		has.setRemoveWhenFarAway(false);
		
		Slime hs = loc.getWorld().spawn(loc, Slime.class);
		hs.setCustomName("n");
		hs.setCustomNameVisible(false);
		hs.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 10000000, 1));
		hs.setSize(-1);
		hs.addPassenger(has);
		hs.setAI(false);
		
		has.addPassenger(s);
		s.setRemoveWhenFarAway(false);
		as.setRemoveWhenFarAway(false);
		customEnt.getBukkitEntity().addPassenger(hs);
		((CraftLivingEntity) customEnt.getBukkitEntity()).setRemoveWhenFarAway(true);
		mcWorld.addEntity(customEnt, SpawnReason.CUSTOM);
		return customEnt.getUniqueID();
	}
}
