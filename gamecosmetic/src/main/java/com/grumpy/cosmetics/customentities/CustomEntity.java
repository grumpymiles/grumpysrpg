package com.grumpy.cosmetics.customentities;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Horse;

import com.grumpy.cosmetics.Cosmetic;
import com.grumpy.cosmetics.data.MobData;
import com.grumpy.cosmetics.data.MobInventoryData;

public class CustomEntity {
	
	public static void spawnVillager(Location loc, String name, int level, int health, int meeledamage, int profession, boolean passive, MobInventoryData inventory)
	{
		CustomVillager.spawnEntity(loc, name, level, health, meeledamage, profession, passive, inventory);
	}
	
	public static void spawnZombie(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomZombie.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnPigman(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomPigman.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnPig(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomPig.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnBlaze(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomBlaze.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnGolem(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomGolem.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnSnowman(Location loc, String name, int level, int health, int meeledamage, boolean passive, MobInventoryData inventory)
	{
		CustomSnowman.spawnEntity(loc, name, level, health, meeledamage, passive, inventory);
	}
	
	public static void spawnHorse(Location loc, String name, int level, int health, int meeledamage, Horse.Color breed, boolean passive, MobInventoryData inventory)
	{
		CustomHorse.spawnEntity(loc, name, level, health, meeledamage, breed, passive, inventory);
	}
	
	public static void spawnText(Location loc, String name, int ticks, Cosmetic instance)
	{
		CustomText.spawnEntity(loc, name, ticks, instance);
	}

	public static UUID spawnMob(Location loc, MobData mob) //Mostly used by spawners
	{
		if(mob.getType() == EntityTypes.CUSTOM_ZOMBIE)
		{
			return CustomZombie.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_VILLAGER)
		{
			return CustomVillager.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.getProfession(), mob.isPassive(), mob.getInventory());		
		} else if(mob.getType() == EntityTypes.CUSTOM_HORSE)
		{
			return CustomHorse.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.getBreed(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_PIGMAN)
		{
			return CustomPigman.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_PIG)
		{
			return CustomPig.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_BLAZE)
		{
			return CustomBlaze.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_SNOWMAN)
		{
			return CustomSnowman.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		} else if(mob.getType() == EntityTypes.CUSTOM_GOLEM)
		{
			return CustomGolem.spawnEntity(loc, mob.getName(), mob.getLevel(), mob.getHealth(), mob.getMeeleDamage(), mob.isPassive(), mob.getInventory());
		}
		return null; //Invalid location or invalid mob
	}	
}