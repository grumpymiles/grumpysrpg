package com.grumpy.cosmetics.customentities;

import java.lang.reflect.Field;
import java.util.List;

import com.grumpy.cosmetics.util.ReflectionUtils;

import net.minecraft.server.v1_12_R1.BiomeBase;
import net.minecraft.server.v1_12_R1.Entity;
import net.minecraft.server.v1_12_R1.EntityArmorStand;
import net.minecraft.server.v1_12_R1.EntityBlaze;
import net.minecraft.server.v1_12_R1.EntityHorse;
import net.minecraft.server.v1_12_R1.EntityInsentient;
import net.minecraft.server.v1_12_R1.EntityIronGolem;
import net.minecraft.server.v1_12_R1.EntityLiving;
import net.minecraft.server.v1_12_R1.EntityPig;
import net.minecraft.server.v1_12_R1.EntityPigZombie;
import net.minecraft.server.v1_12_R1.EntitySnowman;
import net.minecraft.server.v1_12_R1.EntityVillager;
import net.minecraft.server.v1_12_R1.EntityZombie;
import net.minecraft.server.v1_12_R1.MinecraftKey;
import net.minecraft.server.v1_12_R1.RegistryID;
import net.minecraft.server.v1_12_R1.RegistryMaterials;

public enum EntityTypes {
	
	CUSTOM_VILLAGER("Villager", 120, EntityVillager.class, CustomVillager.class),
	CUSTOM_ZOMBIE("Zombie", 54, EntityZombie.class, CustomZombie.class),
	CUSTOM_HORSE("Horse", 100, EntityHorse.class, CustomHorse.class),
	CUSTOM_PIGMAN("Zombie Pigman", 57, EntityPigZombie.class, CustomPigman.class),
	CUSTOM_GOLEM("Iron Golem", 99, EntityIronGolem.class, CustomGolem.class),
	CUSTOM_BLAZE("Blaze", 61, EntityBlaze.class, CustomBlaze.class),
	CUSTOM_SNOWMAN("Snow Golem", 97, EntitySnowman.class, CustomSnowman.class),
	CUSTOM_PIG("Pig", 90, EntityPig.class, CustomPig.class),
	CUSTOM_ARMORSTAND("Armor Stand", 30, EntityArmorStand.class, CustomText.class, false),
	CUSTOM_IRONGOLEMGAURD("Iron Golem", 99, EntityIronGolem.class, CustomIronGolemGaurd.class);
	
	private String name;
    private int id;
    private MinecraftKey minecraftKey;
    private Class<? extends EntityInsentient> nmsClass;
    private Class<? extends EntityInsentient> customClass;
    
    EntityTypes(String name, int id, Class<? extends EntityInsentient> nmsClass, Class<? extends EntityInsentient> customClass) {
		this.name = name;
		this.id = id;
		this.minecraftKey = new MinecraftKey(name);
		this.nmsClass = nmsClass;
		this.customClass = customClass;
    }
    
	EntityTypes(String name, int id, Class<? extends EntityLiving> nmsClass, Class<? extends EntityLiving> customClass, boolean i) { //boolean here because cant have two constructors with same params
		this.name = name;
		this.id = id;
		this.minecraftKey = new MinecraftKey(name);
		this.nmsClass = (Class<? extends EntityInsentient>) nmsClass;
		this.customClass = (Class<? extends EntityInsentient>) customClass;
	}

	
	
	public String getName() {
        return name;
    }

    public int getID() {
        return id;
    }
	
    public MinecraftKey getMinecraftKey() {
		return this.minecraftKey;
	}
    
	public Class<? extends EntityInsentient> getCustomClass() {
        return customClass;
    }
	
	public Class<? extends EntityInsentient> getNMSClass() {
        return nmsClass;
    }
	
	public static void registerEntities() {
		for (EntityTypes entity : values()) {
			try {
				RegistryID<Class<? extends Entity>> registryID = (RegistryID<Class<? extends Entity>>) ReflectionUtils.getPrivateField(RegistryMaterials.class, net.minecraft.server.v1_12_R1.EntityTypes.b, "a");
				Object[] idToClassMap = (Object[]) ReflectionUtils.getPrivateField(RegistryID.class, registryID, "d");
				
				Object oldValue = idToClassMap[entity.getID()];
				registryID.a(entity.getCustomClass(), entity.getID());
				
				idToClassMap[entity.getID()] = oldValue;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for (BiomeBase biomeBase : (Iterable<BiomeBase>) BiomeBase.i) {
			if (biomeBase == null)
				break;
			for (String field : new String[]{ "t", "u", "v", "w" })
				try {
					Field list = BiomeBase.class.getDeclaredField(field);
					list.setAccessible(true);
					List<BiomeBase.BiomeMeta> mobList = (List<BiomeBase.BiomeMeta>) list
							.get(biomeBase);
					
					for (BiomeBase.BiomeMeta meta : mobList)
						for (EntityTypes entity : values())
							if (entity.getNMSClass().equals(meta.b))
								meta.b = entity.getCustomClass();
				} catch (Exception e) {
					e.printStackTrace();
				}
		}
	}
	
	public static void unregisterEntities() {
		for (EntityTypes entity : values()) {
			try {
				net.minecraft.server.v1_12_R1.EntityTypes.b.a(entity.getID(), entity.getMinecraftKey(), entity.getNMSClass());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
