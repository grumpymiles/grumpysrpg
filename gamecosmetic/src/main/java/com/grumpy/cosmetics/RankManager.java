package com.grumpy.cosmetics;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class RankManager implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onChat(AsyncPlayerChatEvent e) {
		e.setFormat("%1$s: %2$s"); // No idea how it works just decompiled a plugin and copied
	}

	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		if (Cosmetic.mysqlenable) {
			Player player = event.getPlayer();
			DataBaseManager.createPlayer(player.getUniqueId());
			DataBaseManager.setRank(player.getUniqueId(), DataBaseManager.getRank(player.getUniqueId()));
			player.setDisplayName(getPrefix(DataBaseManager.getRank(player.getUniqueId()), player.getName()));
		}
	}

	public static String getPrefix(int rankid, String name) {
		switch (rankid) {
		case 0:
			return ChatColor.GRAY + name + ": " + ChatColor.GRAY;
		case 1:
			return ChatColor.GRAY + "[" + ChatColor.YELLOW + "GameMaster" + ChatColor.GRAY + "] " + ChatColor.GRAY + name
					+ "" + ChatColor.WHITE;
		case 2:
			return ChatColor.GRAY + "[" + ChatColor.GREEN + "Builder" + ChatColor.GRAY + "] " + ChatColor.GRAY + name
					+ "" + ChatColor.WHITE;
		case 3:
			return ChatColor.GRAY + "[" + ChatColor.GOLD + "Mod" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + ""
					+ ChatColor.WHITE;
		case 4:
			return ChatColor.GRAY + "[" + ChatColor.BLUE + "Dev" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + ""
					+ ChatColor.WHITE;
		case 5:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Lead Build" + ChatColor.GRAY + "] " + ChatColor.GRAY
					+ name + "" + ChatColor.WHITE;
		case 6:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Lead Dev" + ChatColor.GRAY + "] " + ChatColor.GRAY
					+ name + "" + ChatColor.WHITE;
		case 7:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Admin" + ChatColor.GRAY + "] " + ChatColor.GRAY + name
					+ "" + ChatColor.WHITE;
		default:
			return ChatColor.GRAY + name + ": " + ChatColor.GRAY;
		}
	}
}
