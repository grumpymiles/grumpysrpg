package com.grumpy.BungeePlugin;

import com.grumpy.BungeePlugin.command.CommandFind;
import com.grumpy.BungeePlugin.command.CommandHub;
import com.grumpy.BungeePlugin.command.CommandLobby;
import com.grumpy.BungeePlugin.command.CommandServer;

import net.md_5.bungee.api.plugin.Plugin;

public class BungeePlugin extends Plugin {
	
	
	public void onEnable()
	{	
		//setupMySQL();
		getProxy().getPluginManager().registerCommand(this, new CommandFind());
		getProxy().getPluginManager().registerCommand(this, new CommandLobby());
		getProxy().getPluginManager().registerCommand(this, new CommandServer());
		getProxy().getPluginManager().registerCommand(this, new CommandHub());
		
	}
}
