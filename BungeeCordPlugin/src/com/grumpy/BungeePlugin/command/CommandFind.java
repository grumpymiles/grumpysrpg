package com.grumpy.BungeePlugin.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandFind extends Command {

	public CommandFind()
	{
	    super("find");
	}
	
	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args)
	{
		if(args.length != 1)
		{
			sender.sendMessage(ChatColor.RED + "Invalid Arguments: /find [player]");
		} else {
			
			ProxiedPlayer player = ProxyServer.getInstance().getPlayer(args[0]);
			if ((player == null) || (player.getServer() == null)) {
				sender.sendMessage(ChatColor.DARK_RED + args[0] + ChatColor.RED + " is not online on any servers!");
			} else {
				sender.sendMessage(ChatColor.GREEN + args[0] + " is online at " + player.getServer().getInfo().getName());
			}
		}
	}
	
}
