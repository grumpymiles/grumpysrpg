package com.grumpy.BungeePlugin.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class CommandLobby extends Command {

	public CommandLobby()
	{
	    super("lobby");
	}
	
	public void execute(CommandSender sender, String[] args)
	{
		if(sender instanceof ProxiedPlayer){
			ProxiedPlayer player = (ProxiedPlayer) sender;
			if(!player.getServer().getInfo().getName().equals("l")) { // lobby server is called 'l'
				ServerInfo target = ProxyServer.getInstance().getServerInfo("l");
				player.connect(target);
			} else {
				player.sendMessage(new ComponentBuilder("You are already connected to this server!").color(ChatColor.RED).create());
			}
		}
	}
	
}