package com.grumpy.cosmeticlobby;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.messaging.PluginMessageRecipient;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class LobbyListener implements Listener {
	
	static Lobby instance;
	static int integer = 0;
	
	@SuppressWarnings("static-access")
	public LobbyListener(Lobby instance)
	{
		this.instance = instance;
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		event.getPlayer().getInventory().clear();
		if(!event.getPlayer().getInventory().contains(ItemUtil.createItem(Material.COMPASS, (byte) 0x0, "§aServerSelector"))) {
			event.getPlayer().getInventory().setItem(0, ItemUtil.createItem(Material.COMPASS, (byte) 0x0, "§aServerSelector")); //add the item if player doesnt have one
		}
		Location loc = new Location(Bukkit.getWorld("world"), -760, 46, 213, -180, 0);
		event.getPlayer().teleport(loc);
		event.setJoinMessage("");
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
    public void onInteractEvent(PlayerInteractEvent event) {
		if(event.getItem() != null 
				&& event.getItem().getType() != Material.AIR 
				&& event.getItem().hasItemMeta() 
				&& event.getItem().getItemMeta().hasDisplayName() 
				&& event.getItem().getItemMeta().getDisplayName().equals("§aServerSelector")) {
			event.setCancelled(true);
			openServerSelector(event.getPlayer());
		}
	}
	
	private static void openServerSelector(Player p)
	{
    	List<String> servers = Lobby.getServersList();
		Inventory inv = Bukkit.createInventory(null, 54, "ServerSelector");
		integer = servers.size() - 2;
		for(String server : servers) 
		{
			if(server.startsWith("g")) {
				
				ArrayList<String> lore = new ArrayList<String>();
				lore.add(ChatColor.GRAY + "Players (" + instance.playerCount.get(server) + "/60)");
				inv.setItem(integer, ItemUtil.createItem(Material.STAINED_CLAY, (byte) 5, ChatColor.DARK_GREEN + "Connect to Server " + server.charAt(1), lore));
				
				
				integer--;						    	
			}
		}
		p.openInventory(inv);
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent event) //after player dies
	{
		if(!event.getPlayer().getInventory().contains(ItemUtil.createItem(Material.COMPASS, (byte) 0x0, "§aServerSelector"))) {
			event.getPlayer().getInventory().setItem(0, ItemUtil.createItem(Material.COMPASS, (byte) 0x0, "§aServerSelector")); //add the item if player doesnt have one
		}
	}
	
	@EventHandler
	public void onPlayerHungerChangeEvent(FoodLevelChangeEvent event)
	{
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent event)
	{
		if (event.getCurrentItem() != null && event.getCurrentItem().getType() == Material.COMPASS) {
            event.setCancelled(true);
        }
		if(event.getInventory().getTitle().equals("ServerSelector"))
		{
			event.setCancelled(true);
			
			if (event.getCurrentItem() == null || !event.getCurrentItem().hasItemMeta()) return;
	        if (event.getCurrentItem().getItemMeta().hasDisplayName()) {
	        	if(event.getCurrentItem().getItemMeta().getDisplayName().contains("Connect to Server ")) {
	        		ByteArrayDataOutput out = ByteStreams.newDataOutput();
	        		out.writeUTF("Connect");
	        		out.writeUTF("g" + event.getCurrentItem().getItemMeta().getDisplayName().charAt(event.getCurrentItem().getItemMeta().getDisplayName().length() - 1));
	        		((PluginMessageRecipient) event.getWhoClicked()).sendPluginMessage(instance, "BungeeCord", out.toByteArray());
	        	}
	        }
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if(!(event.getSpawnReason() == SpawnReason.CUSTOM || event.getSpawnReason() == SpawnReason.SPAWNER_EGG)) {
           event.setCancelled(true);
        }
    }
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event)
	{
		if(event.toWeatherState())
			event.setCancelled(true);
	}
}
