package com.grumpy.cosmeticlobby;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

public class Lobby extends JavaPlugin implements PluginMessageListener {

	static List<String> serverList = new ArrayList<String>();
	HashMap<String, Integer> playerCount = new HashMap<String, Integer>();

	public static Connection connection;

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		setupMySQL();

		getServer().getPluginManager().registerEvents(new LobbyListener(this), this);
		getServer().getPluginManager().registerEvents(new RankManager(), this);

		this.getCommand("ninjarankset").setExecutor(new CommandRankSet());

		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		this.getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", this);

		final BukkitRunnable countdownRunnable = new BukkitRunnable() {
            @Override
            public void run() { //every second update the serverlist and player count
            	if(Bukkit.getOnlinePlayers().size() != 0)
            	{
            		ByteArrayDataOutput out = ByteStreams.newDataOutput();
            		out.writeUTF("GetServers");
            		((Player) Bukkit.getOnlinePlayers().toArray()[0]).sendPluginMessage(getPlugin(), "BungeeCord", out.toByteArray());
            		Bukkit.getScheduler().scheduleAsyncRepeatingTask(getPlugin(), new Runnable() {
            			public void run() {
            				if(Bukkit.getOnlinePlayers().size() != 0)
                        	{
	            		    	List<String> servers = Lobby.getServersList();
	            		    	if(!servers.isEmpty())
	            		    	{
		            				for(String server : servers) 
		            				{
		            					if(server.startsWith("g")) {
		            						String servername = server;
		            						ByteArrayDataOutput out = ByteStreams.newDataOutput();
		            						out.writeUTF("PlayerCount");
		            						out.writeUTF(servername);
		            						((Player) Bukkit.getOnlinePlayers().toArray()[0]).sendPluginMessage(getPlugin(), "BungeeCord", out.toByteArray());
		            					}
		            				}
	            		    	}
                        	}
            			}
            		}, 0L, 40L); // 2 seconds
            	}
            }
        };
        countdownRunnable.runTaskTimerAsynchronously(this, 0, 40L); // one minute
	}

	@Override
	public void onDisable() {
	}

	public static List<String> getServersList() {
		return serverList;
	}

	public HashMap<String, Integer> getPlayerCounts() {
		return playerCount;
	}

	@Override
	public void onPluginMessageReceived(final String channel, final Player player, final byte[] message) {
		if (!channel.equals("BungeeCord")) {
			return;
		}
		ByteArrayDataInput in = ByteStreams.newDataInput(message);
		String subchannel = in.readUTF();
		if (subchannel.equals("PlayerCount")) {
			playerCount.put(in.readUTF(), in.readInt());
		} else if (subchannel.equals("GetServers")) {
			String[] servers = in.readUTF().split(", ");
			serverList.clear();
			for (String server : servers)
				serverList.add(server);
		}
	}

	public void setupMySQL() {
		try {
			if (connection != null && !connection.isClosed()) {
				return;
			}

			synchronized (this) {
				if (connection != null && !connection.isClosed()) {
					return;
				}
				Class.forName("com.mysql.jdbc.Driver");
				connection = DriverManager.getConnection("jdbc:mysql://" + "127.0.0.1" + ":" + 3306 + "/" + "maindata"
						+ "?autoReconnect=true&useSSL=false", "root", "");
				Bukkit.getServer().getConsoleSender().sendMessage("Lobby Plugin Connected to SQL database!");
				createTable();
			}
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public void createTable() {
		Statement statement = null;
		try {

			DatabaseMetaData dbm = connection.getMetaData();
			ResultSet tables = dbm.getTables(null, null, "playerrank", null);
			// Get all the current tables

			if (!tables.next()) {
				// If there are no tables (resultset 'tables' is empty)

				statement = connection.createStatement();
				String createtable = "CREATE TABLE IF NOT EXISTS " + "playerrank"
						+ "(UUID varchar(36), RANK int) ENGINE=InnoDB";
				statement.execute(createtable);
				// Configure the table and execute the statement
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static Plugin getPlugin() {
		return Bukkit.getPluginManager().getPlugin("Lobby"); // get plugin
																// through name
	}

}
