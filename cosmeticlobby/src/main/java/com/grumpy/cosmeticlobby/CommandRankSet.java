package com.grumpy.cosmeticlobby;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class CommandRankSet implements CommandExecutor, TabCompleter {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
        	Player player = (Player) sender;
    		if(args.length > 1) {
    			if(player.getName().equals("ninja2003")) {
	    			if(Bukkit.getPlayer(args[0]) != null) {
	    				RankManager.setRank(Bukkit.getPlayer(args[0]).getUniqueId(), Integer.parseInt(args[1]));
	    				player.sendMessage("rank set! ");
	    				Bukkit.getPlayer(args[0]).setDisplayName(RankManager.getPrefix(RankManager.getRank(Bukkit.getPlayer(args[0]).getUniqueId()), Bukkit.getPlayer(args[0]).getName()));
	    				return true;
	    			}
    			}
    			player.sendMessage("Unknown command. Type \"/help\" for help.");
            	return true;
    		}
    		player.sendMessage("too little arguments");
    		return true;
        	
        }
        return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("ninjarankset") && args.length == 0)
		{
			if(sender instanceof Player)
			{
				List<String> list = new ArrayList<>();
				for(Player p : Bukkit.getOnlinePlayers())
				{
					list.add(p.getName());
				}
				return list;
			}
		}
		return null;
	}
}
