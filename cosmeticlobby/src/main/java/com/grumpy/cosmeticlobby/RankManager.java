package com.grumpy.cosmeticlobby;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

public class RankManager implements Listener {
	
	
	@EventHandler(priority=EventPriority.MONITOR)
	public void onChat(AsyncPlayerChatEvent e)
	{
	    e.setFormat("%1$s: %2$s"); //No idea how it works just decompiled a plugin and copied
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		createPlayer(player.getUniqueId(), player);
		setRank(player.getUniqueId(), getRank(player.getUniqueId()));
		player.setDisplayName(getPrefix(getRank(player.getUniqueId()), player.getName()));
	}
	
	public static String getPrefix(int rankid, String name)
	{
		switch(rankid) {
		case 0:
			return ChatColor.GRAY + name + ": " + ChatColor.GRAY;
		case 1:
			return ChatColor.GRAY + "[" + ChatColor.YELLOW + "Game Master" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 2: 
			return ChatColor.GRAY + "[" + ChatColor.GREEN + "Builder" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 3:
			return ChatColor.GRAY + "[" + ChatColor.GOLD + "Mod" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 4:
			return ChatColor.GRAY + "[" + ChatColor.BLUE + "Dev" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 5:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Lead Build" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 6:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Lead Dev" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		case 7:
			return ChatColor.GRAY + "[" + ChatColor.DARK_RED + "Admin" + ChatColor.GRAY + "] " + ChatColor.GRAY + name + "" + ChatColor.WHITE;
		default:
			return ChatColor.GRAY + name + ": " + ChatColor.GRAY;
		}
	}
	
	public boolean playerExists(UUID uuid)
	{
		try {
			PreparedStatement statement = Lobby.connection.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			
			ResultSet results = statement.executeQuery();
			if(results.next()) {
				return true;
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void createPlayer(final UUID uuid, Player player) {
		try {
			PreparedStatement statement = Lobby.connection.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();
			if(playerExists(uuid) != true) {
				PreparedStatement insert = Lobby.connection.prepareStatement("INSERT INTO " + "playerrank" + "(UUID,RANK) VALUE (?,?)");
				insert.setString(1, uuid.toString());
				insert.setInt(2, 0); //set player to default rank
				insert.executeUpdate();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateRank(UUID uuid) {
		try {
			PreparedStatement statement = Lobby.connection.prepareStatement("UPDATE " + "playerrank" + " SET RANK=? WHERE UUID=?");
			statement.setInt(1, 0);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static int getRank(UUID uuid) {
		try {
			PreparedStatement statement = Lobby.connection.prepareStatement("SELECT * FROM " + "playerrank" + " WHERE UUID=?");
			statement.setString(1, uuid.toString());
			ResultSet results = statement.executeQuery();
			results.next();
			
			return results.getInt("RANK");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0; // default rank is 0
	}
	
	public static void setRank(UUID uuid, int rank) {
		try {
			PreparedStatement statement = Lobby.connection.prepareStatement("UPDATE " + "playerrank" + " SET RANK=? WHERE UUID=?");
			statement.setInt(1, rank);
			statement.setString(2, uuid.toString());
			statement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
