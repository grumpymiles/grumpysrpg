package com.grumpy.cosmeticlobby;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

public class ItemUtil {

	
	@SuppressWarnings("deprecation")
	public static ItemStack createItem(Material mat, byte data, String displayName, List<String> lore)
	{
		ItemStack item = new MaterialData(mat, data).toItemStack(1);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(displayName);
		if(lore != null) {
			List<String> finalLore = new ArrayList<>();
            for (String s : lore)
                finalLore.add(s);
            meta.setLore(finalLore);
		}
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		return item;
	}
	
	public static ItemStack createItem(Material mat, byte data, String displayName)
	{
		return createItem(mat, data, displayName, null);
	}
}
