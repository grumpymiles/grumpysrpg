package com.grumpy.buildserver;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Snowman;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockIgniteEvent;
import org.bukkit.event.block.BlockIgniteEvent.IgniteCause;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.EntityBlockFormEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WorldListener implements Listener 
{

	Build instance;
	
	public WorldListener(Build instance)
	{
		this.instance = instance;
	}
	
	@EventHandler
	public void onEntityCombustEvent(EntityCombustEvent event)
	{
		if(event.getEntityType() == EntityType.ZOMBIE)
		{
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockGrowEvent(BlockGrowEvent event)
	{
		event.setCancelled(true); // does not cancel grass growth. It cancels wheat, cactus, watermelon growth etc
	}
	
	@EventHandler
	public void onPlayerJoinEvent(PlayerJoinEvent event)
	{
		instance.rankmanager.updatePlayer(event.getPlayer());
		event.getPlayer().setResourcePack("https://download.nodecdn.net/containers/nodecraft/minepack/cb889a29a7074a7a692ef1681d8457bf.zip");
	}
	
	@EventHandler
	public void onPlayerChatEvent(AsyncPlayerChatEvent event)
	{
		event.setFormat(event.getPlayer().getDisplayName() + ": "+ ChatColor.WHITE + event.getMessage());
	}
	
	@EventHandler
	public void onEntityExplodeEvent(EntityExplodeEvent event) {
		event.setCancelled(true); //prevent grief
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPlaceEvent(BlockPlaceEvent event)
	{
		if(event.getPlayer().hasPermission("build.buildperms"))
		{
			event.setBuild(true);
		} else {
			event.setBuild(false);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerBreakEvent(BlockBreakEvent event)
	{
		if(event.getPlayer().hasPermission("build.buildperms"))
		{
			event.setCancelled(false);
		} else {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event)
	{
		if(event.toWeatherState())
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onLeaveDecay(LeavesDecayEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockFade(BlockFadeEvent event) {
		event.setCancelled(true);
	}
	
	@EventHandler(priority = EventPriority.HIGH)
    public void onCreatureSpawn(CreatureSpawnEvent event) {
        if(!(event.getSpawnReason() == SpawnReason.CUSTOM || event.getSpawnReason() == SpawnReason.SPAWNER_EGG)) {
           event.setCancelled(true);
        }
    }
	
	@EventHandler
	public void onBlockBurn(BlockBurnEvent event)
	{
		event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockSpread(BlockSpreadEvent event)
	{
		event.setCancelled(true); //grass, fire, etc
	}
	
	@EventHandler
	public void onBlockFormEvent(BlockFormEvent event)
	{
		event.setCancelled(true); //Obsidian, snow form storm, etc
	}
	
	@EventHandler
	public void onBlockIgnite(BlockIgniteEvent event) {
		if (event.getCause() != IgniteCause.FLINT_AND_STEEL) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onSnowForm(EntityBlockFormEvent e)
	{
		if(e.getEntity() instanceof Snowman)
			if(e.getNewState().getType() == Material.SNOW)
				e.setCancelled(true);
	}
	
}
