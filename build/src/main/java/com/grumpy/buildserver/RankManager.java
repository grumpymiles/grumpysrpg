package com.grumpy.buildserver;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;

public class RankManager {

	
	Build instance;
	
	public RankManager(Build instance)
	{
		this.instance = instance;
	}
	
	public int getKarma(Player p) {
		return instance.config.getInt("karma." + p.getUniqueId().toString() + ".points");
	}
	
	public void setKarma(Player p, int value ) {
		instance.config.set("karma." + p.getUniqueId().toString() + ".points", value);
		instance.saveConfig();
		return;
	}
	
	public void addKarma(Player p, int value) {
		instance.config.set("karma." + p.getUniqueId().toString() + ".points", value+=getKarma(p));
		instance.saveConfig();
		return;
	}
	
	public void updatePlayer(Player p) {
		int value = 0;
		if(instance.config.contains("karma." + p.getUniqueId().toString() + ".points"))
		{
			value = instance.config.getInt("karma." + p.getUniqueId().toString() + ".points");
		}
		if(value == 0) {
			p.setDisplayName(ChatColor.YELLOW + "(0)" + ChatColor.GRAY + "[Guest]" + p.getName());
			
		} else if(value == 1) {
			p.setDisplayName(ChatColor.YELLOW + "(1)" + ChatColor.GREEN + "[NewBuilder I]" + ChatColor.GRAY + p.getName());
		} else if(value >= 4000)
		{
			p.setDisplayName(ChatColor.DARK_RED + "[Lead Builder]" + ChatColor.GRAY + p.getName());
			PermissionAttachment attachment = p.addAttachment(instance);
			attachment.setPermission("build.leader.addkarma", true);
		} else if(value >= 5000)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.GOLD + "[Legend]" + ChatColor.GRAY + p.getName());
			PermissionAttachment attachment = p.addAttachment(instance);
			attachment.setPermission("build.leader.addkarma", true);
		} else if(value >= 1400)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.BLUE + "[ProBuilder II]" + ChatColor.GRAY + p.getName());
		} else if(value >= 1100)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.BLUE + "[ProBuilder I]" + ChatColor.GRAY + p.getName());
		} else if(value >= 900)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.LIGHT_PURPLE + "[Experienced III]" + ChatColor.GRAY + p.getName());
		} else if(value >= 770)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.LIGHT_PURPLE + "[Experienced II]" + ChatColor.GRAY + p.getName());
		} else if(value >= 610)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.LIGHT_PURPLE + "[Experienced I]" + ChatColor.GRAY + p.getName());
		} else if(value >= 470)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.DARK_GREEN + "[Builder III]" + ChatColor.GRAY + p.getName());
		} else if(value >= 330)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.DARK_GREEN + "[Builder II]" + ChatColor.GRAY + p.getName());
		} else if(value >= 200)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.DARK_GREEN + "[Builder I]" + ChatColor.GRAY + p.getName());
			PermissionAttachment attachment = p.addAttachment(instance);
			attachment.setPermission("",true);
		} else if(value >= 120)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.GREEN + "[NewBuilder III]" + ChatColor.GRAY + p.getName());
		} else if(value >= 60)
		{
			p.setDisplayName(ChatColor.YELLOW + "(" + value + ")" + ChatColor.GREEN + "[NewBuilder II]" + ChatColor.GRAY + p.getName());
		}
		if(value >= 1) {
			PermissionAttachment attachment = p.addAttachment(instance);
			attachment.setPermission("worldedit.*",true);
			attachment.setPermission("voxelsniper.sniper",true);
			attachment.setPermission("voxelsniper.brush.*",true);
			attachment.setPermission("voxelsniper.*",true);
			attachment.setPermission("build.buildperms", true);
			attachment.setPermission("minecraft.command.say", true);
			attachment.setPermission("minecraft.command.tp", true);
			attachment.setPermission("minecraft.command.time", true);
			attachment.setPermission("buildserver.setwarp", true);
			attachment.setPermission("fawe.admin", true);
		}
	}
}
