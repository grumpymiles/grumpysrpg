package com.grumpy.buildserver;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.grumpy.buildserver.command.CommandGamemode;
import com.grumpy.buildserver.command.CommandSetWarp;
import com.grumpy.buildserver.command.CommandSpeed;
import com.grumpy.buildserver.command.CommandWarp;

public class Build extends JavaPlugin {
	
	public FileConfiguration config = getConfig();
	RankManager rankmanager;
	@Override
    public void onEnable() 
	{
		rankmanager = new RankManager(this);
		this.getCommand("speed").setExecutor(new CommandSpeed());
		
		this.getCommand("setwarp").setExecutor(new CommandSetWarp(this));
		this.getCommand("warp").setExecutor(new CommandWarp(this));
		this.getCommand("gm").setExecutor(new CommandGamemode());
		this.getCommand("addkarma").setExecutor(new CommandAddKarma(this));
		
		getServer().getPluginManager().registerEvents(new WorldListener(this), this);
	}

	@Override
    public void onDisable() {}
	
	public static Plugin getPlugin()
	{
		return Build.getPlugin(Build.class); //get plugin through name
	}
}
