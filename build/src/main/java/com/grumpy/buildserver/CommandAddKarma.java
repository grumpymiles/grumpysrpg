package com.grumpy.buildserver;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class CommandAddKarma implements CommandExecutor, TabCompleter {

	Build instance;
	
	public CommandAddKarma(Build instance)
	{
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender.isOp() || sender.hasPermission("build.leader.addkarma"))
		{
			if(args.length >= 1) {
				if(Bukkit.getPlayer(args[0]) != null)
				{
					UUID uuid = Bukkit.getPlayer(args[0]).getUniqueId();
					if(instance.config.contains("karma." + uuid.toString() + ".points"))
					{
						if(StringUtils.isNumeric(args[1]))
						{
							instance.rankmanager.addKarma(Bukkit.getPlayer(args[0]), Integer.parseInt(args[1]));
							instance.rankmanager.updatePlayer(Bukkit.getPlayer(args[0]));
							for(Player p : Bukkit.getOnlinePlayers())
							{
								p.sendMessage(ChatColor.GOLD + "" + args[1] + ChatColor.GREEN + " Karma Has Been Added To " + args[0] + "s Account.");
							}
							return true;
						}
						sender.sendMessage(ChatColor.DARK_RED + "Karma amount must be a number!");
						return true;
					} else {
						if(StringUtils.isNumeric(args[1]))
						{
							instance.rankmanager.setKarma(Bukkit.getPlayer(args[0]), Integer.parseInt(args[1]));
							instance.rankmanager.updatePlayer(Bukkit.getPlayer(args[0]));
							for(Player p : Bukkit.getOnlinePlayers())
							{
								p.sendMessage(ChatColor.GOLD + "" + Integer.parseInt(args[1]) + ChatColor.GREEN + " Karma Has Been given To " + args[0] + "");
							}
							return true;
						}
						sender.sendMessage(ChatColor.DARK_RED + "Karma amount must be a number!");
						return true;
					}
				}
			}
			sender.sendMessage(ChatColor.DARK_RED + "Too little arguments!");
			return true;
		}
		sender.sendMessage("Unknown command. Type \"/help\" for help.");
		return true;
	}
	
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("addkarma") && args.length == 0)
		{
			if(sender instanceof Player)
			{
				List<String> list = new ArrayList<>();
				for(Player p : Bukkit.getOnlinePlayers())
				{
					list.add(p.getName());
				}
				return list;
			}
		}
		return null;
	}

}
