package com.grumpy.buildserver.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class CommandGamemode implements CommandExecutor, TabCompleter {
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player) {
        	Player p = (Player) sender;
    		if(args.length > 0) {
    			if (args[0].equals("0")) {
    				p.setGameMode(GameMode.SURVIVAL);
    				p.sendMessage("Your game mode has been updated");
    				return true;
    			} else if (args[0].equals("1")) {
    				p.setGameMode(GameMode.CREATIVE);
    				p.sendMessage("Your game mode has been updated");
    				return true;
    			} else if (args[0].equals("2")) {
    				p.setGameMode(GameMode.ADVENTURE);
    				p.sendMessage("Your game mode has been updated");
    				return true;
    			} else if (args[0].equals("3")) {
    				p.setGameMode(GameMode.SPECTATOR);
    				p.sendMessage("Your game mode has been updated");
    				return true;
    			} else {
    				p.sendMessage("Invaild agruments");
    				return true;
    			}
    			
    		}
    		p.sendMessage("too little arguments");
    		return true;
        	
        }
        return true;
	}
	
	@Override
	public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("gm") && args.length == 0)
		{
			if(sender instanceof Player)
			{
				List<String> list = new ArrayList<>();
				list.add("0");
				list.add("1");
				list.add("2");
				list.add("3");
				return list;
			}
		}
		return null;
	}
}
