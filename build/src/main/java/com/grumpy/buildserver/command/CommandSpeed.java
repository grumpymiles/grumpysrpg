package com.grumpy.buildserver.command;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandSpeed implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 0) {
				if(args[0].matches("^-?\\d+$")) { // stackoverflows weird code
					if (player.isFlying()) {
						player.setFlySpeed((float) ((double) Integer.parseInt(args[0]) / 10));
						player.sendMessage(ChatColor.GOLD + "Setting fly speed to: " + ChatColor.GREEN + Integer.parseInt(args[0]));
					} else {
						player.setWalkSpeed((float) ((double) Integer.parseInt(args[0]) / 10));
						player.sendMessage(ChatColor.GOLD + "Setting walk speed to: " + ChatColor.GREEN + Integer.parseInt(args[0]));
					}
				}
			}
		}
		return true;
	}
}
