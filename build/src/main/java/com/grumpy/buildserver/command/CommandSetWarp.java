package com.grumpy.buildserver.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.grumpy.buildserver.Build;

public class CommandSetWarp  implements CommandExecutor {

	Build instance;
	
	public CommandSetWarp(Build instance)
	{
		this.instance = instance;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if(player.hasPermission("buildserver.setwarp") || player.isOp())
			{
				if (args.length > 0) 
				{
					if(!instance.config.contains("warps." + args[0]))
					{
						Location loc = player.getLocation();
						instance.config.set("warps." + args[0] + ".location", loc);
						instance.config.set("warps." + args[0] + ".creator", player.getName());
						instance.saveConfig();
						player.sendMessage(ChatColor.DARK_GREEN + args[0] + "Warp created!");
						return true;
					} else 
					{
						player.sendMessage(ChatColor.RED + "Warp name already exists!");
						return true;
					}
				} else 
				{
					player.sendMessage(ChatColor.RED + "Not enough arguments!");
					return true;
				}
			}
		}
		return true;
	}
}
