package com.grumpy.buildserver.command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.grumpy.buildserver.Build;

public class CommandWarp implements CommandExecutor {

	Build instance;
	
	public CommandWarp(Build instance)
	{
		this.instance = instance;
	}
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length > 0) {
				if(instance.config.contains("warps." + args[0]))
				{
					Location loc;
					loc = (Location) instance.config.get("warps." + args[0] + ".location");
					player.teleport(loc);
					player.sendMessage(ChatColor.DARK_GREEN + "You have been teleported to " + ChatColor.GOLD + args[0]);
				} else {
					player.sendMessage(ChatColor.RED + "Warp name doesnt exists!");
				}
			} else {
				player.sendMessage(ChatColor.GOLD + "List of Warps:");
				for(String warpname : instance.config.getConfigurationSection("warps").getKeys(false))
				{
					player.sendMessage(ChatColor.GOLD + "- " + warpname + ChatColor.GREEN + " Creator: " + instance.config.getString("warps." + warpname + ".creator"));
					
				}
				return true;
			}
		}
		return true;
	}

}
